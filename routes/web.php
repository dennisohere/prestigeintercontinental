<?php


use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

Route::prefix('setup')->group(function(){
    Route::get('storage', 'SetupController@storage');

    Route::get('install', 'SetupController@install');

    Route::get('live', 'SetupController@live');

    Route::get('optimization/clear', 'SetupController@clearOptimization');
});
