<?php

namespace Modules\Frontend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Modules\Apps\Repositories\MenuRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class FrontendController extends Controller
{
    public function loadPage(Request $request)
    {
        $path = Str::start($request->path(),'/');

        $active_menu = MenuRepository::init()->getMenuByPath($path);

        if(!$active_menu){
            throw new NotFoundHttpException('Menu could not be loaded.');
        }

        $page = $active_menu->getPageForMenu();

        if(!$page){
            throw new NotFoundHttpException('Page Not Available');
        }

        return view('pages::pages.' . $page->getPageDirectoryName() . '.index', compact('page', 'active_menu'));
    }
}
