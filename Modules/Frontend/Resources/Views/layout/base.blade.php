<!DOCTYPE html>
<html>
<head>
    <title>{{config('app.name')}}</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1">
    <link rel="icon" href="/assets/images/favicon.png">
    <!--fonts-->
    <link href="/assets/fonts/et-line-font/style.css" rel="stylesheet">
    <link href="/assets/fonts/elegant_font/css/style.css" rel="stylesheet">
    <!--styles-->
    <link rel="stylesheet" href="/assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="/assets/css/owl.carousel.min.css"/>

    <link rel="stylesheet" href="/assets/css/style.css"/>
    <link rel="stylesheet" href="/assets/css/responsive.css"/>
    <!--script-->
    <script src="/assets/js/jquery.min.js" type="text/javascript"></script>
    <!--<script src="js/bootstrap.min.js" type="text/javascript"></script>-->
    <script src="/assets/js/bootstrap.js" type="text/javascript"></script>
    <script src="/assets/js/wow.min.js" type="text/javascript"></script>
    <script src="/assets/js/mousescroll.js" type="text/javascript"></script>
    <script src="/assets/js/bootstrap-hover-dropdown.js" type="text/javascript"></script>
    <script src="/assets/js/owl.carousel.min.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.countTo.js" type="text/javascript"></script>
    <script src="/assets/js/shuffle.min.js" type="text/javascript"></script>
    <script src="/assets/js/shuffle.custom.js" type="text/javascript"></script>
    <script src="/assets/js/jquery.imagesloaded.js" type="text/javascript"></script>
    <script src="/assets/js/masonry.pkgd.min.js" type="text/javascript"></script>
    <script src="/assets/js/magnific-popup.js" type="text/javascript"></script>
    <script src="/assets/js/custom.js" type="text/javascript"></script>

    @stack('styles')


    @if(isset($edit_mode) && $edit_mode)
        <link rel="stylesheet" type="text/css" href="/assets/backend/vendors/contenttools/content-tools.min.css">
    @endif
    <style>
        [data-fixture] > img {
            display: none;
        }
    </style>

    <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/icon/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/icon/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/icon/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/icon/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/icon/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/icon/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/icon/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/icon/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/icon/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/icon/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/icon/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/icon/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/icon/favicon/favicon-16x16.png">
    <link rel="manifest" href="/assets/images/icon/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/assets/images/icon/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#a8518a">
</head>
<body>
<div id="tv-page">
    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="tv-object" id="object_one"></div>
                <div class="tv-object" id="object_two"></div>
                <div class="tv-object" id="object_three"></div>
                <div class="tv-object" id="object_four"></div>
            </div>
        </div>
    </div>
    @include('frontend::partials.navbar')

    @yield('base')

    @include('frontend::partials.footer')
    <a href="#" id="back-to-top" class="back-to-top  arrow_up "></a>
</div>

@stack('scripts')


@if(isset($edit_mode) && $edit_mode)
    <script src="/assets/backend/vendors/contenttools/content-tools.min.js"></script>
    <script src="/assets/backend/vendors/contenttools/editor.js"></script>
@endif
</body>
</html>
