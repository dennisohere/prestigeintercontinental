@php

$menus = \Modules\Apps\Repositories\MenuRepository::init()->getOnlyParentMenus();

@endphp
<nav class="navbar navbar-default navbar-fixed-top tv-nav-transparent tv-font-white tv-sticky-nav nav-border-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-2 pull-left">
                <div class="tv-logo">
                    <a class="tv-logo-light" href="/">
                        <img alt="" style="max-height: 55px; background-color: #ffffff78; border: none;"
                             src="{{\Modules\Backend\Repositories\SettingsRepository::init()->getLogo()}}" class="logo img-thumbnail">
                    </a>
                    <a class="tv-logo-dark" href="/">
                        <img alt="" style="max-height: 55px;" src="{{\Modules\Backend\Repositories\SettingsRepository::init()->getLogo()}}" class="logo">
                    </a>
                </div>
            </div>
            <div class="navbar-header col-sm-8 col-xs-2 pull-right">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            </div>
            <div class="col-md-10 tv-step-menu text-right accordion-menu">
                <div class="navbar-collapse collapse">
                    <ul id="accordion" class="nav navbar-nav navbar-right panel-group">
                        @foreach($menus as $menu)
                        <li class="dropdown panel {{$menu->has_children() ? 'simple-dropdown' : ''}}"
                        style="position: relative;">
                            <a href="@if($menu->has_children())#menu-{{$menu->id}}@else {{$menu->getAccessibleLink()}} @endif"
                               class="{{$menu->has_children() ? 'dropdown-toggle collapsed' : ''}}"
                            @if($menu->has_children()) data-toggle="collapse" data-parent="#accordion" data-hover="dropdown" @endif>
                                {{$menu->title}}
                            </a>

                            @if($menu->has_children())
                                <ul id="menu-{{$menu->id}}" class="dropdown-menu panel-collapse collapse"
                                style="top: 47px; left: 18px;">
                                    @foreach($menu->getChildren() as $sub_menu)
                                        <li><a href="{{$sub_menu->getAccessibleLink()}}">{{$sub_menu->title}}</a></li>
                                    @endforeach
                                </ul>
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>