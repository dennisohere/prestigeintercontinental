<section class="tv-primary-features tv-bg-red tv-white-text tv-padd-tb-40">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-4 col-xs-12 clearfix tv-col-3-wrap">
                <div class="feature feature-icon-small text-center">
                    <i class="icon icon-global"></i>
                    @if($edit_mode)
                        <div data-editable data-name=feature-area-heading-1>
                            {!! $content['feature-area-heading-1'] ?? '<h3 class="tv-white-text text-capitalize">
                                    Disruptors
                                </h3>' !!}
                        </div>
                        <div data-editable data-name=feature-area-heading-content-1>
                            {!! $content['feature-area-heading-content-1'] ?? '<p class="text-white">
                                    Our goal is to build societies of the future, improve quality of lives,
                                    and create wealth through subsidiaries and ventures that leverage partnerships with experts...
                                </p>' !!}
                        </div>
                    @else
                        {!! $content['feature-area-heading-1'] ?? '<h3 class="tv-white-text text-capitalize">
                                Disruptors
                            </h3>' !!}

                        {!! $content['feature-area-heading-content-1'] ?? '<p class="text-white">
                                Our goal is to build societies of the future, improve quality of lives,
                                and create wealth through subsidiaries and ventures that leverage partnerships with experts...
                            </p>' !!}
                    @endif
                </div><!--end of feature-->
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 clearfix tv-col-3-wrap">
                <div class="feature feature-icon-small text-center">
                    <i class="icon icon-global"></i>
                    @if($edit_mode)
                        <div data-editable data-name=feature-area-heading-2>
                            {!! $content['feature-area-heading-2'] ?? '<h3 class="tv-white-text text-capitalize">
                                    Change Catalysts
                                </h3>' !!}
                        </div>
                        <div data-editable data-name=feature-area-heading-content-2>
                            {!! $content['feature-area-heading-content-2'] ?? '<p class="text-white">
                                    We are determined to bring innovation into various aspects of industry and economy
                                    with global outlook and local relevance. We have our eyes on the future...
                                </p>' !!}
                        </div>
                    @else
                        {!! $content['feature-area-heading-2'] ?? '<h3 class="tv-white-text text-capitalize">
                                Change Catalysts
                            </h3>' !!}

                        {!! $content['feature-area-heading-content-2'] ?? '<p class="text-white">
                                    We are determined to bring innovation into various aspects of industry and economy
                                    with global outlook and local relevance. We have our eyes on the future...
                                </p>' !!}
                    @endif
                </div><!--end of feature-->
            </div>
            <div class="col-md-4 col-sm-4 col-xs-12 clearfix tv-col-3-wrap">
                <div class="feature feature-icon-small text-center">
                    <i class="icon icon-global"></i>
                    @if($edit_mode)
                        <div data-editable data-name=feature-area-heading-3>
                            {!! $content['feature-area-heading-3'] ?? '<h3 class="tv-white-text text-capitalize">
                                    Solution Providers
                                </h3>' !!}
                        </div>
                        <div data-editable data-name=feature-area-heading-content-3>
                            {!! $content['feature-area-heading-content-3'] ?? '<p class="text-white">
                                    Our eyes are ever open to the opportunities every challenge presents and we are poised
                                    to make the most of the opportunities with local content and global relevance.
                                </p>' !!}
                        </div>
                    @else
                        {!! $content['feature-area-heading-3'] ?? '<h3 class="tv-white-text text-capitalize">
                                    Solution Providers
                                </h3>' !!}

                        {!! $content['feature-area-heading-content-3'] ?? '<p class="text-white">
                                    Our eyes are ever open to the opportunities every challenge presents and we are poised
                                    to make the most of the opportunities with local content and global relevance.
                                </p>' !!}
                    @endif
                </div><!--end of feature-->
            </div>
        </div>
    </div>
</section>