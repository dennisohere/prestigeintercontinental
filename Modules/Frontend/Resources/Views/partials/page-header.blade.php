
<section  class="tv-page-title tv-fixed-img tv-padd-tb-150"
          @if(!$edit_mode)
          style="position: relative; background-image: url({{$images['page-cover-image'][0] ?? '/assets/images/architecture-featured1.jpg'}})"
        @endif
>

    @if($edit_mode)
        <div style="position: absolute; top : 0; width: 100%;
                padding: 187px 0;
                background-image: url({{$images['page-cover-image'][0] ?? '/assets/images/architecture-featured1.jpg'}})"
             class="tv-page-title tv-fixed-img"
             data-fixture
             data-name=page-cover-image
             data-ce-tag="img-fixture">
            <img src="{{$images['page-cover-image'][0] ?? '/assets/images/architecture-featured1.jpg'}}" alt="">
        </div>
        @else
        <div class="tv-opacity-light tv-black"></div>
    @endif
    <div class="container tv-position-relative">
        <div class="row">
            <div class="col col-xs-12">
                <h1 class="tv-white-text tv-alt-font tv-font-22 tv-letter-spacing-2">
                    {{$page->getPageTitle()}}
                </h1>
                <ol class="breadcrumb">
                    <li><a href="/">Home</a></li>
                    @if(isset($active_menu))
                        @php
                            $parent_menu = $active_menu->parent_menu
                        @endphp
                        @while($parent_menu)
                            <li><a href="{{$parent_menu->getAccessibleLink()}}">{{$parent_menu->title}}</a></li>
                            @php
                                $parent_menu = $parent_menu->parent_menu
                            @endphp
                        @endwhile
                    @endif
                    <li class="active tv-light-white-text">
                        {{$page->getPageTitle()}}
                    </li>
                </ol>
            </div>
        </div> <!-- end row -->
    </div>
</section>