@php

    $menus = \Modules\Apps\Repositories\MenuRepository::init()->getOnlyParentMenus();

@endphp
<section class="">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 tv-bg-red-light">
                <div class="tv-padd-tb-40 text-center">
                    <i class="icon-phone icon-small tv-white-text"></i>
                    <h6 class="tv-offspace-top-5 tv-offspace-botttom-00 tv-gray-light-text">
                        +234 8068031163
                    </h6>
                </div>
            </div>
            {{--<div class="col-md-6 col-sm-6 tv-bg-red">
                <div class="tv-padd-tb-40 text-center">
                    <i class="icon-map-pin icon-small tv-white-text"></i>
                    <h6 class="tv-offspace-top-5 tv-offspace-botttom-00 tv-gray-light-text">
                        701 Old York Drive Richmond USA.
                    </h6>
                </div>
            </div>--}}
            <div class="col-md-6 col-sm-6 tv-bg-red">
                <div class="tv-padd-tb-40 text-center">
                    <i class="icon-envelope icon-small tv-white-text"></i>
                    <h6 class="tv-offspace-top-5 tv-offspace-botttom-00 tv-gray-light-text">
                        <a href="mailto:info@prestigeintercontinental.com"> info@prestigeintercontinental.com</a>
                    </h6>
                </div>
            </div>
        </div>
    </div>
</section>
<footer id="footer4">
    <div class="tv-bg-dark-gray clearfix tv-padd-tb-30">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12 text-left text-small display-table xs-margin-15px-bottom equal-height tv-xs-equal-height-auto">
                    <div class="display-table-cell vertical-align-middle copyright tv-light-gray1-text">
                        &copy; {{date('Y')}} All right reserved.
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 display-table text-right sm-text-center xs-margin-15px-bottom equal-height tv-xs-equal-height-auto">
                    <div class="display-table-cell vertical-align-middle">
                        <div class="footer-logo logo pull-right">
                            <a href="/">
                                <img alt="" style="max-height: 55px; background-color: #ffffff78; border: none;"
                                     src="{{\Modules\Backend\Repositories\SettingsRepository::init()->getLogo()}}" class="logo img-thumbnail">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>