
@php

$slides = \Modules\Apps\Repositories\SlidesRepository::init()->getSlides();
$random_slide = $slides->count() > 0 ? $slides->random()->first() : null;

@endphp

@if($random_slide)
    <section id="slider" class="tv-no-padd">
        <div class="tv-slider tv-slider-one owl-carousel">
            <article class="tv-slider-item">
                <div class="tv-owl-slider-img" style="background-image: url({{$random_slide->image_url}})">
                    <div class="tv-opacity-light tv-bg-dark-gray"></div>
                    <div class="container tv-position-relative tv-full-screen">
                        <div class="tv-slider-content text-center">
                            <div class="tv-slider-content-main">
                                <div class="tv-slider-content-middle tv-padd-lr-15 text-center">
                                    {{--<h1 class="tv-white-text tv-alt-font tv-font-weight-300 text-unset">--}}
                                        {{--{{$random_slide->title}}--}}
                                    {{--</h1>--}}
                                    <p class="text-lowercase text-center"
                                    style="
                                    font-style: italic;
                                    font-size: 2.7em;max-width: 220px;
                                    margin: auto;
                                    color: white;line-height: 1.5em;
                                    text-transform: uppercase;text-align: center;">
                                        {{$random_slide->caption}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
    </section>
@endif

{{--
<section id="slider" class="tv-no-padd">
    <div class="tv-slider tv-slider-one owl-carousel tv-line-dots">
        @if($slides->count() > 0)
            @foreach($slides as $slide)
                <article class="tv-slider-item">
                    <div class="tv-owl-slider-img" style="background-image: url({{$slide->image_url}})">
                        <div class="tv-opacity-medium tv-bg-dark-gray"></div>
                        <div class="container tv-position-relative tv-full-screen">
                            <div class="tv-slider-content text-center">
                                <div class="tv-slider-content-main">
                                    <div class="tv-slider-content-middle">
                                        <h1 class="tv-slider-title tv-white-text">
                                            {{$slide->title}}
                                        </h1>
                                        <p class="tv-slider-sub-title tv-offspace-botttom-2-5">
                                            {{$slide->caption}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            @endforeach
        @else
        <article class="tv-slider-item">
            <div class="tv-owl-slider-img" style="background-image: url(/assets/images/onepage-slider-img2.jpg)">
                <div class="tv-opacity-medium tv-bg-dark-gray"></div>
                <div class="container tv-position-relative tv-full-screen">
                    <div class="tv-slider-content text-center">
                        <div class="tv-slider-content-main">
                            <div class="tv-slider-content-middle">
                                <h1 class="tv-slider-title tv-white-text">
                                    welcome...
                                </h1>
                                <p class="tv-slider-sub-title tv-offspace-botttom-2-5">
                                    we are '{{config('app.name')}}'
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        @endif

    </div>
</section>--}}
