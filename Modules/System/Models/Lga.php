<?php

namespace Modules\System\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class Lga extends Model
{
//    use HasMetadataAttribute;

    protected $table = 'lgas';

    protected $guarded =['id'];

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
}
