<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 27 Sep 2019
 * Time: 9:08 AM
 */

namespace Modules\System\Repositories;


use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\SystemRepositoryTrait;

abstract class BaseRepository
{
    use SystemRepositoryTrait;

    /**
     * @var Model
     */
    protected $model;

    /**
     * @var array
     */
    private $payload;

    /**
     * List of laravel model events that should be automatically listened to
     *
     * @var array
     */
    protected $model_events = [];

    /**
     * @param Model $model
     * @throws \Exception
     */
    public function setModel(Model $model): void
    {

        $this->model = $model;

        foreach ($this->model_events as $event){
            $handler = $event . '_handler';

            if(method_exists($this, $handler)){
                $this->model::$event(function(Model $model) use ($handler){
                    $this->$handler($model);
                });
            } else {
                throw new \Exception(new \Exception('Method ' . '"' . $handler . '" does not exist!'));
            }
        }

    }

    /**
     * @return Model
     */
    public function getModel(): Model
    {
        return $this->model;
    }

    public function getModelById($id)
    {
        return $this->model->newModelQuery()->find($id);
    }

    /**
     * @param array $payload
     * @return BaseRepository
     * @throws \Exception
     */
    public function initSave(array $payload)
    {
        $this->payload = $payload;

        $edit = isset($this->payload['id']);

        $model = $edit ? $this->getModelById($this->payload['id']) : $this->model->newInstance();

        $this->setModel($model);

        return $this;
    }

    /**
     * @param \Closure|null $custom_fill_model
     * @return $this
     */
    public function fillModel(\Closure $custom_fill_model = null)
    {

        if($custom_fill_model){
            /**
             * @var \Closure $custom_fill_model
             */
            $this->model = $custom_fill_model($this->model, $this->payload);
        } else {
            $this->model->fill($this->payload);
        }

        return $this;
    }

    /**
     * provide the keys to uploaded files this format ['file_request_key_name' => 'model column name']
     *
     * @param array $uploads
     * @return $this
     */
    public function doUpload(array $uploads = [])
    {
        foreach ($uploads as $request_file_key => $model_column){

            if ($uploaded = $this->autoSaveFileFromRequest($request_file_key)){

                $this->model->fill([
                    $model_column => $uploaded['url']
                ]);

                if(method_exists($this->model, 'fillMetadata')){
                    $this->model->fillMetadata([$model_column => $uploaded]);
                }

            }
        }

        return $this;
    }

    public function persist()
    {
        $this->model->save();

        return $this->model;

    }

    /**
     * @param array $payload
     * @param \Closure|null $actions
     * @return Model
     * @throws \Exception
     */
    public function quickSave(array $payload, \Closure $actions = null)
    {
        $this->initSave($payload);

        if($actions){
            $actions($this);
        } else {
            $this->fillModel();
        }

        return $this->persist();
    }

    public function fetch($limit = null, \Closure $filterQuery = null)
    {
        $query = $this->model->newModelQuery();

        if($filterQuery){
            $query = $filterQuery($query);
        }

        return $limit ? $query->latest()->paginate($limit) : $query->latest()->get();
    }

    /**
     * @param \Closure|null $filterQuery
     * @return \Illuminate\Database\Eloquent\Builder|Model|mixed
     */
    public function fetchQuery(\Closure $filterQuery = null)
    {
        $query = $this->model->newModelQuery();

        if($filterQuery){
            $query = $filterQuery($query);
        }

        return $query->latest();
    }

    /**
     * @param Model $model
     * @throws \Exception
     */
    public function deleteByModel(Model $model)
    {
        $model->delete();
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function deleteById($id)
    {
        $model = $this->getModelById($id);

        $this->deleteByModel($model);

    }
}