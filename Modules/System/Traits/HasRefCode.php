<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 9/19/2019
 * Time: 7:49 AM
 */

namespace Modules\System\Traits;


use Illuminate\Support\Str;

trait HasRefCode
{

    public function fillRefCode()
    {
        return $this->fill([
            'ref_code' => $this->generateRefCode()
        ]);
    }

    public function getRefCode()
    {
        return $this->ref_code;
    }

    public function generateRefCode()
    {

        return Str::random();

    }

    public static function fetchByRefCode($ref_code)
    {
        return self::where('ref_code', $ref_code)->first();
    }
}