<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 6/13/2019
 * Time: 10:23 AM
 */

namespace Modules\System\Traits;


use Modules\Backend\Repositories\SettingsRepository;

trait HasAvatar
{

    public function getAvatar($avatar_column_name = 'image')
    {
        return $this->$avatar_column_name ?? SettingsRepository::init()->getLogo();
    }

    public function getUploadConfigByColumn($column = 'image')
    {
        return $this->metadata[$column] ?? null;
    }

    public function getAvatarStoragePath($column_name = 'image')
    {
        return $this->getUploadConfigByColumn($column_name)['path'] ?? null;
    }
}