<section class="tv-testimonial tv-slider-test tv-section-padding">
    <div class="container tv-padd-tb-100">
        <div class="row">
            <div class="col-md-8 center-col text-center tv-divider-line tv-offspace-botttom-50">
                <div class="tv-subheader tv-bg-white">
                    <h2 class="text-uppercase tv-header tv-font-weight-600">Partners</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-loaded owl-drag tv-dark-dots">
                    @foreach(\Modules\Apps\Repositories\PartnerRepository::init()->fetch() as $partner)
                    <div class="tv-team-img">
                        <img class="" src="{{$partner->image}}" alt="image">
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <script>
            $('.tv-slider-test .owl-carousel').owlCarousel({
                loop: true,
                items: 4,
                margin: 0,
                nav: true,
                dots: true,
                autoplay: true,
                autoplayTimeout: 1000,
                autoplayHoverPause: true,
                autoplaySpeed: 1000,
                navSpeed: 1000,
                singleItem: true,
                navText: ["<i class='arrow_carrot-left'></i>", "<i class='arrow_carrot-right'></i>"]
            });
        </script>
    </div>
</section>