<section class="tv-blog tv-padd-tb-100 tv-light-gray">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center tv-offspace-botttom-40">
                <h2 class="tv-header tv-font-weight-600">
                    <span class="tv-font-90per tv-font-weight-400 tv-red-text text-capitalize tv-letter-spacing-2">
                            Projects
                        </span>
                </h2>
            </div>
        </div>
        <div class="row">
            @foreach(\Modules\Apps\Repositories\ProjectRepository::init()->getProjects()->random(3) as $project)
                <div class="col-md-4 col-sm-4">
                    <div class="tv-blog-post">

                        <div class="tv-blog-post-image"
                             style="background-color: #626262;
                                     margin: 10px;
                                     background-repeat: no-repeat;
                                     background-position: center;
                                     background-image: url({{$project->getCoverImage()}}); background-size: cover">
                            <div class="tv-letter-spacing-3 tv-line-height-30"
                                 style="color: white; padding: 55px; background-color: rgba(0,0,0,0.68); height: 430px;">
                                <p class="">
                                    {{$project->title}}
                                </p>
                                <p>
                                            <span class="tv-bg-red tv-padd-all-4per">
                                                {{$project->category->title}}
                                            </span>
                                </p>
                            </div>

                        </div>

                    </div>

                </div>
            @endforeach

            <div class="col-md-12 text-center">
                <a href="/our-work/projects" class="btn btn-primary">View more...</a>
            </div>
        </div>
    </div>
</section>