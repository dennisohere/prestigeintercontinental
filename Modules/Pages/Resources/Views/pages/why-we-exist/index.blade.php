@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-elements tv-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    @if($edit_mode)

                        <div data-editable data-name=page-quote>
                            {!! $content['page-quote'] ?? '<blockquote class="tv-offspace-tb-5 tv-off-black-text">
                        <p class="tv-extra-dark-gray-text">Sed vitae fermentum diam, eget sollicitudin felis. Vestibulum massa       	  arcu, tempor sit amet mollis ac, eleifend auctor turpis Pellentesque feu giat quis turpis quis bibendum</p>
                        <footer><cite title="Source Title">Source Title</cite></footer>
                    </blockquote>' !!}
                        </div>

                    @else

                        {!! $content['page-quote'] ?? '<blockquote class="tv-offspace-tb-5 tv-off-black-text">
                        <p class="tv-extra-dark-gray-text">Sed vitae fermentum diam, eget sollicitudin felis. Vestibulum massa       	  arcu, tempor sit amet mollis ac, eleifend auctor turpis Pellentesque feu giat quis turpis quis bibendum</p>
                        <footer><cite title="Source Title">Source Title</cite></footer>
                    </blockquote>' !!}

                    @endif

                </div>
            </div>
        </div>
    </section>

    <section id="about" class="tv-padd-tb-50">
        <div class="container">
            <div class="row">
                <div class="col-md-8 center-col tv-offspace-bottom-6">
                    @if($edit_mode)
                        <div data-editable data-name=why-we-exist-content>
                            {!! $content['why-we-exist-content'] ?? '
                            <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                        </p>
                            ' !!}
                        </div>
                    @else
                        {!! $content['why-we-exist-content'] ?? '
                            <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                        </p>
                            ' !!}
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-projects')

@endsection