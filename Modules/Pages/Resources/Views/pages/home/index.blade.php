@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.slides')

    @include('frontend::partials.feature-section')

    <section class="tv-side-image-info">
        <div class="tv-image-container col-md-5 col-sm-4 pull-left">
            <div data-fixture
                 data-name=we-are-prestige-image
                 data-ce-tag="img-fixture"
                 class="background-image-left-fix"
                 style="background: url({{$images['we-are-prestige-image'][0] ?? '/assets/images/side2.jpg'}})
                 no-repeat scroll 65% 45%; background-position: 50% 50%;
                 background-size: cover;"
            >
                <img src="{{$images['we-are-prestige-image'][0] ?? '/assets/images/side2.jpg'}}" alt="">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6 col-sm-7 col-sm-offset-5 tv-content-wrap tv-padd-tb-80 clearfix">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=we-are-prestige-heading>
                                    {!! $content['we-are-prestige-heading'] ?? '<h2 class="tv-offspace-botttom-30">
                                            We are
                                            <span class="tv-font-90per tv-font-weight-700 tv-red-text text-capitalize tv-letter-spacing-2">
                                                Prestige
                                            </span>
                                        </h2>' !!}
                                </div>
                                <div data-editable data-name=we-are-prestige-content>
                                    {!! $content['we-are-prestige-content'] ?? '<p class="lead tv-offspace-botttom-30">
                                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                        </p>' !!}
                                </div>

                            @else
                                {!! $content['we-are-prestige-heading'] ?? '<h2 class="tv-offspace-botttom-30">
                                            We are
                                            <span class="tv-font-90per tv-font-weight-700 tv-red-text text-capitalize tv-letter-spacing-2">
                                                Prestige
                                            </span>
                                        </h2>' !!}

                                {!! $content['we-are-prestige-content'] ?? '<p class="lead tv-offspace-botttom-30">
                                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                        </p>' !!}

                            @endif
                        </div>
                    </div>
                    <div class="row tv-offspace-top-50">
                        <div class="col-sm-6">
                            <h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-30 tv-offspace-xs-bottom-15">
                                Our Vision
                            </h4>
                            @if($edit_mode)
                                <div data-editable data-name=our-vision-snippet>
                                    {!! $content['our-vision-snippet'] ?? '<p>
                                To provide sustainable solutions to world economic problems of the now and tomorrow...
                            </p>' !!}
                                </div>
                            @else
                                {!! $content['our-vision-snippet'] ?? '<p>
                                To provide sustainable solutions to world economic problems of the now and tomorrow...
                            </p>' !!}
                            @endif
                        </div>

                        <div class="col-sm-6">
                            <h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-30 tv-offspace-xs-bottom-15">
                                Our Mission
                            </h4>
                            @if($edit_mode)
                                <div data-editable data-name=our-mission-snippet>
                                    {!! $content['our-mission-snippet'] ?? '<p>
                                    Leverage partnerships and organize resources to solve global problems in local environments in a creative and sustainable manner...
                                </p>' !!}
                                </div>
                            @else
                                {!! $content['our-mission-snippet'] ?? '<p>
                                    Leverage partnerships and organize resources to solve global problems in local environments in a creative and sustainable manner...
                                </p>' !!}
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tv-service-section-main tv-padd-tb-100-60">
        <div class="container">
            <div class="row">
                <div class="col-md-8 center-col text-center tv-divider-line tv-offspace-botttom-50">
                    <div class="tv-subheader tv-bg-white">
                        <h2 class="text-uppercase tv-header tv-font-weight-600">What we do</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 col-xs-12 tv-col-3-wrap">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon-lightbulb"></i>
                        </div>
                        <div class="pull-right">
                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-title-1>
                                    {!! $content['what-we-do-title-1'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Ventures
                            </h4>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-title-1'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Ventures
                            </h4>' !!}
                            @endif

                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-description-1>
                                    {!! $content['what-we-do-description-1'] ?? '<p class="tv-letter-spacing-1">
                                Our approach to global problem solving is captured in turning challenges to
                                opportunities, and opportunities to business ventures in select sectors.
                            </p>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-description-1'] ?? '<p class="tv-letter-spacing-1">
                                Our approach to global problem solving is captured in turning challenges to
                                opportunities, and opportunities to business ventures in select sectors.
                            </p>' !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 tv-col-3-wrap">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon-lightbulb"></i>
                        </div>
                        <div class="pull-right">

                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-title-2>
                                    {!! $content['what-we-do-title-2'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Consulting
                            </h4>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-title-2'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Consulting
                            </h4>' !!}
                            @endif

                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-description-2>
                                    {!! $content['what-we-do-description-2'] ?? '<p class="tv-letter-spacing-1">
                                The rapid and dynamic global economic landscape requires timely and effective business intelligence.
                                Prestige provides research based advisory services...
                            </p>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-description-2'] ?? '<p class="tv-letter-spacing-1">
                                The rapid and dynamic global economic landscape requires timely and effective business intelligence.
                                Prestige provides research based advisory services...
                            </p>' !!}
                            @endif

                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-xs-12 tv-col-3-wrap">
                    <div class="feature feature-icon-large">
                        <div class="pull-left">
                            <i class="icon-lightbulb"></i>
                        </div>
                        <div class="pull-right">
                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-title-3>
                                    {!! $content['what-we-do-title-3'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Training
                            </h4>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-title-3'] ?? '<h4 class="tv-font-weight-600 tv-letter-spacing-1 tv-offspace-botttom-15">
                                Training
                            </h4>' !!}
                            @endif

                            @if($edit_mode)
                                <div data-editable data-name=what-we-do-description-3>
                                    {!! $content['what-we-do-description-3'] ?? '<p class="tv-letter-spacing-1">
                                More than ever, players in key economic sectors need to be increasingly equipped if
                                they must compete successfully and thrive in the global market.
                            </p>' !!}
                                </div>
                            @else
                                {!! $content['what-we-do-description-3'] ?? '<p class="tv-letter-spacing-1">
                                More than ever, players in key economic sectors need to be increasingly equipped if
                                they must compete successfully and thrive in the global market.
                            </p>' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="why-choose-block tv-padd-t-100">
        <div class="tv-fixed-img tv-padd-tb-100" style="background-image: url('/assets/images/abstract-adventure-background-2397652.jpg')">
            <div class="tv-bg-dark-overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 equal-height" style="height: 190px;">
                        <div class="display-table">
                            <div class="display-table-cell vertical-align-middle">
                                <div class="tv-wrap-box tv-white-text">
                                    <h2 class="tv-white-text"> <strong>Why Work With Us?</strong></h2>
                                    @if($edit_mode)
                                        <div data-editable data-name=why-work-with-us-desc>
                                            {!! $content['why-work-with-us-desc'] ?? '<p class="tv-letter-spacing-1 tv-gray-light-text tv-offspace-tb-5">
                                        At Prestige, we see opportunities in every economic challenge and we are positioned to harness those opportunities for growth
                                    </p>' !!}
                                        </div>


                                    @else
                                        {!! $content['why-work-with-us-desc'] ?? '<p class="tv-letter-spacing-1 tv-gray-light-text tv-offspace-tb-5">
                                        At Prestige, we see opportunities in every economic challenge and we are positioned to harness those opportunities for growth
                                    </p>' !!}


                                    @endif


                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-md-offset-2 equal-height tv-white-text" style="height: 190px;">
                        <div class="display-table tv-w-h-100">
                            <div class="display-table-cell vertical-align-middle">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-xs-12 text-center tv-light-text tv-counter-section tv-no-padd-tb">
                                        <i class="icon-happy tv-counter-icon"></i>
                                        <span class="" style="font-size: 40px;font-weight: 600;letter-spacing: 2px;line-height: 60px;">
                                            C
                                        </span>
                                        <h5 class="tv-counter-title tv-light-white-text">
                                            commitment
                                        </h5>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 text-center tv-light-text tv-counter-section tv-no-padd-tb">
                                        <i class="icon-target tv-counter-icon"></i>
                                        <span class="" style="font-size: 40px;font-weight: 600;letter-spacing: 2px;line-height: 60px;">
                                            A
                                        </span>
                                        <h5 class="tv-counter-title tv-light-white-text">
                                            aptitude
                                        </h5>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 text-center tv-light-text tv-counter-section tv-no-padd-tb">
                                        <i class="icon-profile-male tv-counter-icon"></i>
                                        <span class="" style="font-size: 40px;font-weight: 600;letter-spacing: 2px;line-height: 60px;">
                                            N
                                        </span>
                                        <h5 class="tv-counter-title tv-light-white-text">
                                            network
                                        </h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <script>
                            $('.counter').each(count);
                            function count(options) {
                                var $this = $(this);
                                options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                                $this.countTo(options);
                            }
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-partners')
    @include('pages::partials.our-projects')


@endsection