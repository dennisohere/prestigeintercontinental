@extends('frontend::layout.base')

@php

    $edit_mode = isset($edit_mode) ? $edit_mode : null;
    $content = $page ? $page->getPageContent() : [];
    $images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-side-image-info">

        <div class="tv-image-container col-md-5 col-sm-4 pull-left">
            <div data-fixture
                 data-name=core-value-image
                 data-ce-tag="img-fixture"
                 class="background-image-left-fix"
                 style="background: url({{$images['core-value-image'][0] ?? '/assets/images/side2.jpg'}})
                         no-repeat scroll 65% 45%; background-position: 50% 50%;
                         background-size: cover;"
            >
                <img src="{{$images['core-value-image'][0] ?? '/assets/images/side2.jpg'}}" alt="">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6 col-sm-7 col-sm-offset-5 tv-content-wrap tv-padd-tb-80 clearfix">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=core-values-content>
                                    {!! $content['core-values-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['core-values-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tv-about-content tv-light-gray ">
        <div class="container">
            <div class="row">
                <div class="col-md-6 tv-padd-all-5per tv-offspace-botttom-4per">
                    <div class="restaurant-menu-text tv-padd-all-6per">
                        @if($edit_mode)
                            <div data-editable data-name=core-values-list-content>
                                {!! $content['core-values-list-content'] ?? '
                                <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                            </p>
                                ' !!}
                            </div>
                        @else
                            {!! $content['core-values-list-content'] ?? '
                                <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                            </p>
                                ' !!}
                        @endif
                    </div>
                </div>
                <div class="col-md-6 tv-padd-all-5per tv-offspace-botttom-4per">
                    <div class="restaurant-menu-text tv-padd-all-6per">
                        @if($edit_mode)
                            <div data-editable data-name=core-values-list-content-2>
                                {!! $content['core-values-list-content-2'] ?? '
                                <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                            </p>
                                ' !!}
                            </div>
                        @else
                            {!! $content['core-values-list-content-2'] ?? '
                                <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                            </p>
                                ' !!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-partners')

@endsection