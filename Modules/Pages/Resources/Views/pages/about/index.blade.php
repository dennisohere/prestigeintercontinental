@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    @include('frontend::partials.feature-section')

    <section id="about" class="tv-padd-tb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-8 center-col tv-offspace-botttom-6 text-center">
                    {{--<span class="text-uppercase tv-font-14 tv-alt-font tv-letter-spacing-1 tv-red-text">lorem ipsum dolor sit amet</span>--}}
                    {{--<h2 class="text-unset tv-font-weight-600">About us</h2>--}}
                    @if($edit_mode)
                        <div data-editable data-name=we-are-prestige-heading>
                            {!! $content['we-are-prestige-heading'] ?? '<h2 class="tv-offspace-botttom-30">
                                    We are
                                    <span class="tv-font-90per tv-font-weight-700 tv-red-text text-capitalize tv-letter-spacing-2">
                                        Prestige
                                    </span>
                                </h2>' !!}
                        </div>
                    @else
                        {!! $content['we-are-prestige-heading'] ?? '<h2 class="tv-offspace-botttom-30">
                                    We are
                                    <span class="tv-font-90per tv-font-weight-700 tv-red-text text-capitalize tv-letter-spacing-2">
                                        Prestige
                                    </span>
                                </h2>' !!}
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-md-8 center-col tv-offspace-bottom-6">
                    @if($edit_mode)
                        <div data-editable data-name=about-content>
                            {!! $content['about-content'] ?? '
                            <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                        </p>
                            ' !!}
                        </div>
                    @else
                        {!! $content['about-content'] ?? '
                            <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                            At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                        </p>
                            ' !!}
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-projects')

@endsection