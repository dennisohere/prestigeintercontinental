@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

$people = \Modules\Apps\Repositories\PeopleRepository::init()->fetch();

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-padd-tb-50 tv-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-md-7 center-col tv-offspace-bottom-6">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=our-people-content>
                                    {!! $content['our-people-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['our-people-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="tv-padd-tb-100">
        <div class="container">
            <div class="row">
                @foreach($people as $person)
                <div class="col-md-12 col-xs-12 tv-col-2-wrap">
                    <div class="tv-model-details-box clearfix xs-no-margin">
                        <div class="col-md-5 no-padding">
                            <img src="{{$person->image}}" alt="">
                        </div>
                        <div class="col-md-7 tv-no-padd">
                            <div class="tv-model-details">
                                <div class="tv-separator-line tv-black tv-offspace-lr-00"></div>
                                <span class="text-uppercase tv-off-black-text tv-letter-spacing-2 tv-font-weight-600">
                                    {{$person->names}}
                                </span>
                                <span class="text-uppercase text-small letter-spacing-2 tv-display-block tv-red-text tv-font-11 tv-letter-spacing-1">
                                    {{$person->position}}
                                </span>
                                <p class="tv-offspace-tb-10">
                                    {!! nl2br($person->bio) !!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    @include('pages::partials.our-partners')

@endsection