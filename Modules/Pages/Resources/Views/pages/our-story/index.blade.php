@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-side-image-info">

        <div class="tv-image-container col-md-5 col-sm-4 pull-left">
            <div data-fixture
                 data-name=our-story-image
                 data-ce-tag="img-fixture"
                 class="background-image-left-fix"
                 style="background: url({{$images['our-story-image'][0] ?? '/assets/images/side2.jpg'}})
                         no-repeat scroll 65% 45%; background-position: 0% 31%;
                         background-size: contain;"
            >
                <img src="{{$images['our-story-image'][0] ?? '/assets/images/side2.jpg'}}" alt="">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6 col-sm-7 col-sm-offset-5 tv-content-wrap tv-padd-tb-80 clearfix">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)

                                <div data-editable data-name=page-quote>
                                    {!! $content['page-quote'] ?? '<blockquote class="tv-offspace-tb-5 tv-off-black-text">
                                <p class="tv-extra-dark-gray-text">Sed vitae fermentum diam, eget sollicitudin felis. Vestibulum massa       	  arcu, tempor sit amet mollis ac, eleifend auctor turpis Pellentesque feu giat quis turpis quis bibendum</p>
                                <footer><cite title="Source Title">Source Title</cite></footer>
                            </blockquote>' !!}
                                </div>

                            @else

                                {!! $content['page-quote'] ?? '<blockquote class="tv-offspace-tb-5 tv-off-black-text">
                                <p class="tv-extra-dark-gray-text">Sed vitae fermentum diam, eget sollicitudin felis. Vestibulum massa       	  arcu, tempor sit amet mollis ac, eleifend auctor turpis Pellentesque feu giat quis turpis quis bibendum</p>
                                <footer><cite title="Source Title">Source Title</cite></footer>
                            </blockquote>' !!}

                            @endif

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=our-story-content>
                                    {!! $content['our-story-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['our-story-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=our-story-content-2>
                                    {!! $content['our-story-content-2'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['our-story-content-2'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-projects')

@endsection