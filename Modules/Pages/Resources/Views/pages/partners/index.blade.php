@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-light-gray">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6 col-sm-7 col-sm-offset-5 center-col tv-content-wrap tv-padd-tb-80 clearfix">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=partner-page-text>
                                    {!! $content['partner-page-text'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['partner-page-text'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio-main-block tv-grid-3col tv-padd-tb-100">
        <div class="container-fluid">
            <div class="row">
                <div class="tv-grid-gallery tv-offspace-top-30" style="padding: 20px">
                    <ul id="grid" class="tv-offspace-50 portfolio-grid">
                        @foreach(\Modules\Apps\Repositories\PartnerRepository::init()->fetch() as $partner)
                        <li class="item grid-item col-lg-4 center-col col-md-4 col-sm-6 col-xs-12 tv-grid-gutter-00">
                            <div class="tv-blog-post">

                                <div class="tv-blog-post-image"
                                     style="background-color: #f7f7f7;
                                             margin: 10px;
                                             height: 150px;
                                             background-repeat: no-repeat;
                                             background-position: center;
                                             background-image: url({{$partner->getCoverImage()}}); background-size: contain">


                                </div>

                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>


@endsection