@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

$sectoral_works = \Modules\Apps\Repositories\SectoralWorkRepository::init()->fetch();

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-info tv-padd-tb-50">
        <div class="container">
            <div class="row">
                <div class="col-12  tv-offspace-xs-bottom-20">
                    <div class="tv-height-100 hidden-sm hidden-xs clearfix"></div>
                    @foreach($sectoral_works->chunk(3) as $chunk)
                        <div class="row">
                            @foreach($chunk as $sectoral_work)
                                <div class="col-md-4 col-sm-6 col-xs-12 text-center tv-feature-box">
                                    <i class="icon-ribbon tv-font-40 tv-offspace-botttom-30"></i>
                                    <h5 class="tv-font-14 tv-alt-font">
                                        {{$sectoral_work->title}}
                                    </h5>
                                    <p class="tv-offspace-top-15">
                                        {{$sectoral_work->description}}
                                    </p>
                                </div>
                            @endforeach
                        </div>
                        @if(!$loop->last)
                        <hr>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-projects')

@endsection

@push('styles')

    <style>
        div#tv-page section.tv-page-title.tv-fixed-img.tv-padd-tb-150 {
            background-size: contain;
            background-position: 100% 0%;
            background-attachment: initial;
        }
    </style>

@endpush