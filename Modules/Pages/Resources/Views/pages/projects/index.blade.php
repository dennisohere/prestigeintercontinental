@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-light-gray">

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-6 col-sm-7 col-sm-offset-5 center-col tv-content-wrap tv-padd-tb-80 clearfix">

                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            @if($edit_mode)
                                <div data-editable data-name=project-page-text>
                                    {!! $content['project-page-text'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['project-page-text'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-botttom-20">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="portfolio-main-block tv-grid-3col tv-padd-t-100">
        <div class="container-fluid">
            <div class="row">
                <div class="tv-filter-wrapper col-xs-12">
                    <div class="portfolio-filter filter-options text-center">
                        <button class="active" data-group="all">all</button>
                        @foreach(\Modules\Apps\Repositories\ProjectCategoryRepository::init()->getAllCategories() as $category)
                        <button data-group="{{\Illuminate\Support\Str::slug($category->title)}}">{{$category->title}}</button>
                        @endforeach
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="tv-grid-gallery tv-offspace-top-30" style="padding: 20px">
                    <ul id="grid" class="tv-offspace-50 portfolio-grid">
                        @foreach(\Modules\Apps\Repositories\ProjectRepository::init()->getProjects() as $project)
                        <li class="item {{\Illuminate\Support\Str::slug($project->category->title)}} grid-item col-lg-4 col-md-4 col-sm-6 col-xs-12 tv-grid-gutter-00"
                            data-groups='["{{\Illuminate\Support\Str::slug($project->category->title)}}"]'>
                            <div class="tv-blog-post">

                                <div class="tv-blog-post-image"
                                     style="background-color: #626262;
                                             margin: 10px;
                                             background-repeat: no-repeat;
                                             background-position: center;
                                             background-image: url({{$project->getCoverImage()}}); background-size: cover">
                                    <div class="tv-letter-spacing-3 tv-line-height-30" style="color: white; padding: 55px; background-color: rgba(0,0,0,0.68);">
                                        <p class="">
                                            {{$project->title}}
                                        </p>
                                        <p>
                                            <span class="tv-bg-red tv-padd-all-4per">
                                                {{$project->category->title}}
                                            </span>
                                        </p>
                                    </div>

                                </div>

                            </div>
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>


@endsection