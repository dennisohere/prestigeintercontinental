@extends('frontend::layout.base')

@php

$edit_mode = isset($edit_mode) ? $edit_mode : null;
$content = $page ? $page->getPageContent() : [];
$images = count($content) > 0 && $content['images'] ? json_decode($content['images'], true) : [];

@endphp

@section('base')

    @include('frontend::partials.page-header')

    <section class="tv-info tv-padd-tb-100">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 tv-offspace-xs-bottom-20">
                    <div class="tv-height-100 hidden-sm hidden-xs clearfix"></div>

                    <div class="tv-list-block">
                        <div class="media">
                            <div class="media-body media-middle">
                                <h2 class="tv-alt-font-rs tv-font-weight-700 text-lowercase tv-letter-spacing-1 tv-no-margin-bottom">
                                    <a>our<span class="tv-red-text text-uppercase">VISION</span></a>
                                </h2>
                            </div>
                            @if($edit_mode)
                                <div data-editable data-name=vision-content>
                                    {!! $content['vision-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-tb-2">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['vision-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-tb-2">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                    <div class="tv-list-block">
                        <div class="media">

                            <div class="media-body media-middle">
                                <h2 class="tv-alt-font-rs tv-font-weight-700 text-lowercase tv-letter-spacing-1 tv-no-margin-bottom">
                                    <a>our<span class="tv-red-text text-uppercase">MISSION</span></a>
                                </h2>
                            </div>
                            @if($edit_mode)
                                <div data-editable data-name=mission-content>
                                    {!! $content['mission-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-tb-2">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                                </div>
                            @else
                                {!! $content['mission-content'] ?? '
                                    <p class="tv-font-14 tv-letter-spacing-1 tv-offspace-tb-2">
                                    At the core, Prestige Intercontinental is a value-driven holding company focused on profitably solving major world economic problems through ventures, investments and projects in key sectors, including energy, oil & gas, real estate, agriculture, mineral exploration, and health.
                                </p>
                                    ' !!}
                            @endif
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="row tv-offspace-top-20">
                        <div class="col-md-12">
                            <div class="tv-image" style="overflow-y: hidden">
                                <div data-fixture
                                     data-name=mission-vision-image
                                     data-ce-tag="img-fixture"
                                     class="background-image-left-fix"
                                     style="background: url({{$images['mission-vision-image'][0] ?? '/assets/images/side2.jpg'}})
                                             no-repeat scroll 65% 45%; background-position: 50% 50%;
                                             height: 350px;
                                             background-size: cover;"
                                >
                                    <img src="{{$images['mission-vision-image'][0] ?? '/assets/images/side2.jpg'}}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include('pages::partials.our-projects')

@endsection