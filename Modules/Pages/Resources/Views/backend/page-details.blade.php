@extends('backend::layout.app')

@php

    $page_config = $page->getPageConfig();

@endphp

@section('app')

    <div class="row">
        <div class="col-sm-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">{{$page->getPageTitle()}}</h3>
                        </div>
                        <div class="col text-right"></div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="card">

                                <div class="card-header">
                                    <a href="{{route('backend.pages.manage.content', ['page_dir' => $page->getKey()])}}"
                                       target="_blank"
                                       class="btn btn-sm btn-info">Content Editor</a>
                                </div>

                                <div class="">
                                    <iframe class="page-preview-frame"
                                            src="{{route('backend.pages.manage.preview', ['page_dir' => $page->getKey()])}}"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-5">

                            <h5 class="heading-small">Page Config</h5>
                            <hr class="mt-0">

                            <form action="{{route('backend.pages.save-config', ['page_dir' => $page->getKey()])}}" method="post">
                                @csrf

                                <div class="row">

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="title" class="form-control-label">Page Title:</label>
                                            <input type="text" name="title" class="form-control form-control-sm form-control-alternative" value="{{$page->getPageTitle()}}">
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="description" class="form-control-label">Description:</label>
                                            <textarea name="description" id="description" rows="10"
                                                      class="form-control form-control-sm form-control-alternative">{{$page_config['description'] ?? ''}}</textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-secondary btn-sm">Save config</button>
                                        </div>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')

    <style>
        iframe.page-preview-frame {
            width: 614px;
            height: 400px;
            border: none;
            zoom: 0.9;
        }
    </style>

@endpush