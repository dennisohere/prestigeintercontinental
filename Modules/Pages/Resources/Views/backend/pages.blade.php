@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-sm-12 mb-5">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Pages</h3>
                        </div>
                        <div class="col text-right">
                            <a href="#!" class="btn btn-sm btn-primary">See all</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach($pages as $page)
                        <div class="col-sm-4">
                            <div class="card" style="width: 18rem;">
                                <div class="card-img-top">
                                    <iframe class="page-preview-frame"
                                            src="{{route('backend.pages.manage.preview', ['page_dir' => $page->getKey()])}}"></iframe>
                                </div>
                                <div class="card-body">
                                    {{$page->getPageTitle()}}
                                </div>
                                <div class="card-footer">
                                    <a href="{{route('backend.pages.manage', ['page_dir' => $page->getPageDirectoryName()])}}"
                                       class="btn btn-secondary btn-sm">
                                        Manage
                                    </a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('styles')

    <style>
        iframe.page-preview-frame {
            width: 320px;
            height: 200px;
            border: none;

            zoom: 0.9;
        }
    </style>

@endpush