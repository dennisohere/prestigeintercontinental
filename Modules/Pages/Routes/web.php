<?php

use Illuminate\Support\Facades\Route;

Route::prefix('backend/pages')->middleware('auth')->namespace('Backend')->name('backend.pages.')->group(function () {
    Route::get('/', 'PagesController@index')->name('index');
    Route::get('manage/{page_dir}', 'PagesController@manage')->name('manage');

    Route::get('manage/{page_dir}/content', 'PagesController@content')->name('manage.content');
    Route::get('manage/{page_dir}/preview', 'PagesController@preview')->name('manage.preview');
    Route::post('manage/{page_dir}/content', 'PagesController@savePageContent')->name('manage.content');
    Route::post('/upload-image', 'PagesController@uploadImage')->name('manage.upload-image');
    Route::post('/insert-image', 'PagesController@insertImage')->name('manage.insert-image');

    Route::post('manage/{page_dir}/config/save', 'PagesController@savePageConfig')->name('save-config');


});
