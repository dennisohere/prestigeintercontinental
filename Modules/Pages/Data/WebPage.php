<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/15/2019
 * Time: 9:37 AM
 */

namespace Modules\Pages\Data;


use Illuminate\Support\Str;

class WebPage
{
    /**
     * @var string
     */
    public $real_path;


    /**
     * WebPage constructor.
     * @param string $real_path
     */
    public function __construct(string $real_path)
    {
        $this->real_path = $real_path;
    }

    public function getPageTitle()
    {
        $config = $this->getPageConfig();

        return $config['title'] ?? Str::title($this->getPageDirectoryName());
    }

    public function getPageDirectoryName()
    {
        $array = explode(DIRECTORY_SEPARATOR, $this->real_path);

        return $array[count($array)-1];
    }

    public function getKey()
    {
        return $this->getPageDirectoryName();
    }

    public function getConfigPath()
    {
        return $this->real_path . DIRECTORY_SEPARATOR . $this->getConfigFileName();
    }

    public function getPageContent()
    {
        return $this->getPageConfig()['content'] ?? [];
    }

    public function getPageConfig()
    {
        if(!file_exists($this->getConfigPath())){
            // create config file
            $this->generateConfigFile();
        }

        $config_string = file_get_contents($this->getConfigPath());

        $data = json_decode($config_string, true);

        return $data;
    }

    public function savePageContent(array $payload)
    {
        $page_config = $this->getPageConfig();
        $content = $page_config['content'] ?? [];

        $page_content = array_merge($content, $payload);
        $page_config = array_merge($page_config, ['content' => $page_content]);

        $newJsonString = json_encode($page_config, JSON_PRETTY_PRINT);

        file_put_contents($this->getConfigPath(), $newJsonString);

        return $page_config;
    }

    public function savePageConfig(array $payload)
    {
        $page_config = $this->getPageConfig();

        $page_config = array_merge($page_config, $payload);

        $newJsonString = json_encode($page_config, JSON_PRETTY_PRINT);

        file_put_contents($this->getConfigPath(), $newJsonString);

        return $page_config;
    }

    public function generateConfigFile(array $data = [])
    {
        if(!file_exists($this->getConfigPath())) {
            $newJsonString = json_encode($data, JSON_PRETTY_PRINT);

            file_put_contents($this->getConfigPath(), stripslashes($newJsonString));
        }

        return $this->getConfigPath();
    }

    public function getConfigFileName()
    {
        return config('pages_module.page_config_filename');
    }
}