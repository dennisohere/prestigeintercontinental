<?php

namespace Modules\Pages\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Modules\Pages\Repositories\PagesRepository;
use Modules\System\Repositories\FileRepository;

class PagesController extends Controller
{

    public function index()
    {
        $pages = PagesRepository::init()->getPages();
        $pageTitle = 'Web Pages';

        return view('pages::backend.pages', compact('pages', 'pageTitle'));
    }

    public function manage($page_dir)
    {
        $page = PagesRepository::init()->getPageFromDirectory($page_dir);
        $pageTitle = 'Manage Web Page';

        return view('pages::backend.page-details', compact('page', 'pageTitle'));
    }

    public function content($page_dir)
    {
        $page = PagesRepository::init()->getPageFromDirectory($page_dir);

        return view('pages::pages.' . $page_dir . '.index', [
            'edit_mode' => true,
            'page' => $page
        ]);

    }

    public function preview($page_dir)
    {
        $page = PagesRepository::init()->getPageFromDirectory($page_dir);

        return view('pages::pages.' . $page_dir . '.index', [
            'edit_mode' => false,
            'page' => $page
        ]);

    }

    public function savePageContent($page_dir, Request $request)
    {
        $page = PagesRepository::init()->getPageFromDirectory($page_dir);

        $payload = $request->except('_token');

        $page->savePageContent($payload);

        return $payload;
    }

    public function savePageConfig($page_dir, Request $request)
    {
        $page = PagesRepository::init()->getPageFromDirectory($page_dir);

        $payload = $request->except('_token');

        $payload['slug'] = Str::slug($payload['title']);

        $page->savePageConfig($payload);

        flash()->success('Saved!');

        return redirect()->back();
    }

    public function uploadImage(Request $request)
    {
        if($request->hasFile('image')){
            $uploaded_image = $request->file('image');
            $filename = time() . '_' . rand(0, 10) . '_.' . $uploaded_image->clientExtension();

            $path = FileRepository::init()->saveFileAs($uploaded_image, $filename);

            return response()->json([
                'filename' => $filename,
                'path' => $path,
                'url' => '/storage' . $path
            ]);
        }
    }

    public function insertImage(Request $request)
    {
        return $request->except('_token');
    }
}
