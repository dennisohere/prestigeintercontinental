<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/15/2019
 * Time: 9:33 AM
 */

$module = null;
try {
    $module = module_path('pages');
} catch (\Caffeinated\Modules\Exceptions\ModuleNotFoundException $e) {
}

return [
    'pages_root_dir' => $module ? $module .
        DIRECTORY_SEPARATOR . 'Resources' .
        DIRECTORY_SEPARATOR . 'Views' .
        DIRECTORY_SEPARATOR . 'pages' .
        DIRECTORY_SEPARATOR : null,
    'page_config_filename' => 'config.json'
];