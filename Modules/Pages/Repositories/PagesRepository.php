<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/15/2019
 * Time: 9:33 AM
 */

namespace Modules\Pages\Repositories;


use Modules\Pages\Data\WebPage;
use Modules\System\Traits\SystemRepositoryTrait;

class PagesRepository
{
    use SystemRepositoryTrait;

    public function getPages()
    {
        $pages = glob(config('pages_module.pages_root_dir') . '*' , GLOB_ONLYDIR);

        return array_map(function ($page_real_path){
            return new WebPage($page_real_path);
        }, $pages);

    }

    /**
     * @param $directory_name
     * @return WebPage
     */
    public function getPageFromDirectory($directory_name)
    {
        $page_dir_path = glob(config('pages_module.pages_root_dir') . $directory_name, GLOB_ONLYDIR)[0];

        return new WebPage($page_dir_path);
    }

}