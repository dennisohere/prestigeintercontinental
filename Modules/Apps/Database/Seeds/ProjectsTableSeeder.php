<?php

namespace Modules\Apps\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Apps\Repositories\ProjectCategoryRepository;
use Modules\Apps\Repositories\ProjectRepository;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Oil & Gas' => [
                'Commercial Manufacturing/Production of LPG Gas Cylinder & Affordable Stove in partnership with select State Governments.',
                'Gas Distribution, Storage and Transportation Systems.',
                'Gas pipeline construction and operation under public-private partnerships.'
            ],
            'Energy' => [
                'Entrepreneurship and skill acquisition training programme on solar energy enterprise for youths in Northern Nigeria'
            ],
            'Agriculture' => [
                'Complete Knocked Down assembly plant for agricultural equipment and machines in partnership with some state governments, international organizations and patent technology owners.',
                'Cash crop production assisted programme for farmers in local communities, with focus on rice, wheat, sorghum, guinea corn, garlic and soya beans.'
            ],
            'Health' => [
                'Production of Medical Oxygen, Surgical Nitrogen, Oxygen Acetylene and Applicable Cylinder.'
            ]
        ];

        foreach ($categories as $category => $projects){
            $cat = ProjectCategoryRepository::init()->saveCategory(['title' => $category]);

            foreach ($projects as $project){
                ProjectRepository::init()->saveProject($cat, [
                    'title' => $project
                ]);
            }
        }
    }
}
