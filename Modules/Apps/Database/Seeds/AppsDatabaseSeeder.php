<?php

namespace Modules\Apps\Database\Seeds;

use Illuminate\Database\Seeder;

class AppsDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProjectsTableSeeder::class);
    }
}
