<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('title');
            $table->string('path')->nullable();
            $table->string('page_dir')->nullable();
            $table->string('external_url')->nullable();
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->unsignedInteger('order_no')->default('0');

            $table->longText('metadata')->nullable();

            $table->index(['path', 'page_dir']);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
