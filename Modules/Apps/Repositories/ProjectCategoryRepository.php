<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 26 Sep 2019
 * Time: 11:01 AM
 */

namespace Modules\Apps\Repositories;


use Modules\Apps\Models\ProjectCategory;
use Modules\System\Repositories\BaseRepository;
use Modules\System\Traits\SystemRepositoryTrait;

class ProjectCategoryRepository extends BaseRepository
{


    /**
     * ProjectCategoryRepository constructor.
     * @param ProjectCategory $category
     * @throws \Exception
     */
    public function __construct(ProjectCategory $category)
    {
        $this->setModel($category);
    }

    public function getAllCategories()
    {
        return $this->fetch()->all();
    }

    /**
     * @param array $payload
     * @return \Illuminate\Database\Eloquent\Model
     * @throws \Exception
     */
    public function saveCategory(array $payload)
    {

        $category = $this->quickSave(['title' => $payload['title']]);

        return $category;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|ProjectCategory|ProjectCategoryRepository|ProjectCategoryRepository[]
     */
    public function getCategoryById($id)
    {
        return $this->getModelById($id);
    }
}