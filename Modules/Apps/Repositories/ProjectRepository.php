<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 26 Sep 2019
 * Time: 10:49 AM
 */

namespace Modules\Apps\Repositories;


use Modules\Apps\Models\Project;
use Modules\Apps\Models\ProjectCategory;
use Modules\System\Repositories\BaseRepository;
use Modules\System\Repositories\FileRepository;

class ProjectRepository extends BaseRepository
{

    protected $model_events = [
        'deleting',
    ];

    /**
     * ProjectRepository constructor.
     * @param Project $project
     * @throws \Exception
     */
    public function __construct(Project $project)
    {
        $this->setModel($project);
    }

    public function getProjects($limit = null)
    {
        return $this->fetch($limit);
    }

    /**
     * @param ProjectCategory $category
     * @param array $payload
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Project|Project[]|null
     * @throws \Exception
     */
    public function saveProject(ProjectCategory $category, array $payload)
    {
        $project = $this
            ->initSave($payload)
            ->fillModel(function(Project $model, array $data) use ($category){
                return  $model->fill([
                    'title' => $data['title'],
                    'project_category_id' => $category->id
                ]);
            })
            ->doUpload([
                'image' => 'cover_image'
            ])
            ->persist();

        return $project;
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|Project|Project[]|null
     */
    public function getProjectById($id)
    {
        return $this->getModelById($id);
    }

    public function deleting_handler(Project $project)
    {
        FileRepository::init()->deleteFile($project->image);
    }
}