<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 27 Sep 2019
 * Time: 9:23 PM
 */

namespace Modules\Apps\Repositories;


use Modules\Apps\Models\Partner;
use Modules\System\Repositories\BaseRepository;
use Modules\System\Repositories\FileRepository;

class PartnerRepository extends BaseRepository
{
    protected $model_events = [
        'deleting'
    ];

    /**
     * PartnerRepository constructor.
     * @param Partner $partner
     * @throws \Exception
     */
    public function __construct(Partner $partner)
    {
        $this->setModel($partner);
    }

    public function deleting_handler(Partner $partner)
    {
        FileRepository::init()->deleteFile($partner->getAvatarStoragePath());
    }
}