<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 27 Sep 2019
 * Time: 9:23 PM
 */

namespace Modules\Apps\Repositories;


use Illuminate\Support\Str;
use Modules\Apps\Models\People;
use Modules\System\Repositories\BaseRepository;
use Modules\System\Repositories\FileRepository;

class PeopleRepository extends BaseRepository
{
    protected $model_events = [
        'deleting',
        'saving'
    ];

    /**
     * PartnerRepository constructor.
     * @param People $people
     * @throws \Exception
     */
    public function __construct(People $people)
    {
        $this->setModel($people);
    }

    public function deleting_handler(People $people)
    {
        FileRepository::init()->deleteFile($people->getAvatarStoragePath());
    }

    public function saving_handler(People $people)
    {
        $people->fill([
            'excerpt' => Str::limit($people->bio, 200)
        ]);
    }
}