<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/17/2019
 * Time: 3:16 AM
 */

namespace Modules\Apps\Repositories;


use Illuminate\Http\Request;
use Modules\Apps\Models\Slide;
use Modules\System\Repositories\BaseRepository;
use Modules\System\Repositories\FileRepository;

class SlidesRepository extends BaseRepository
{
    protected $model_events = [
        'deleting'
    ];

    /**
     * SlidesRepository constructor.
     * @param Slide $slide
     * @throws \Exception
     */
    public function __construct(Slide $slide)
    {
        $this->setModel($slide);
    }

    public function getSlides()
    {
        return $this->fetch();
    }

    /**
     * @param array $payload
     * @return Slide
     * @throws \Exception
     */
    public function saveSlide(array $payload)
    {
        /** @var Slide $slide */
        $slide = $this
            ->initSave($payload)
            ->fillModel(function(Slide $model, array $data) {
                return  $model->fill([
                    'title' => $data['title'],
                    'caption' => $data['caption'],
                    'link' => $data['link'],
                ]);
            })
            ->doUpload([
                'image' => 'image_url'
            ])
            ->persist();

        return $slide;
    }

    public function deleteSlideByIds(array $delete_ids)
    {
        $this->model::destroy($delete_ids);

    }

    public function deleting_handler(Slide $slide)
    {
        FileRepository::init()->deleteFile($slide->getImageUploadStoragePath());
    }

}