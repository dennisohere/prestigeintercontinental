<?php

namespace Modules\Apps\Repositories;



use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Str;
use Modules\Apps\Models\Menu;
use Modules\System\Repositories\BaseRepository;

class MenuRepository extends BaseRepository
{


    /**
     * MenuRepository constructor.
     * @param Menu $menu
     * @throws \Exception
     */
    public function __construct(Menu $menu)
    {
        $this->setModel($menu);
    }

    /**
     * @param array $payload
     * @return Menu
     * @throws \Exception
     */
    public function saveMenu(array $payload)
    {
        $path = Str::start($payload['path'],'/');

        /** @var Menu $menu */
        $menu = $this
            ->initSave($payload)
            ->fillModel(function(Menu $model, array $data) use ($path) {
                return  $model->fill([
                    'title' => $data['title'],
                    'path' => $path,
                    'page_dir' => $data['page'],
                    'external_url' => $data['link'],
                    'parent_id' => $data['parent'],
                ]);
            })
            ->persist();

        $sub_menu_collection = null;
        if($payload['parent']){
            $sub_menu_collection = $this->getMenusByParentId($payload['parent']);
        }

        $menu->setOrderNumber($payload['order_number'], $sub_menu_collection);

        return $menu;
    }

    public function getAllMenus()
    {
        return $this->fetchQuery(function($query){
            /** @var Builder $query */
            return $query->orderBy('order_no', 'asc');
        })->get();

    }

    public function getByMainMenus()
    {
        return $this->fetchQuery(function($query){
            /** @var Builder $query */
            return $query
                ->whereNull('parent_id')
                ->orderBy('order_no', 'asc')
                ->with('child_menus')
                ;
        })->get();
    }

    public function getOnlyParentMenus()
    {
        return $this->fetchQuery(function($query){
            /** @var Builder $query */
            return $query
                ->whereNull('parent_id')
                ->orderBy('order_no', 'asc')
//                ->with('child_menus')
                ;
        })->get();

    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|Menu
     */
    public function getMenuById($id)
    {
        return $this->getModelById($id);
    }

    /**
     * @param $path
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|Menu|object
     */
    public function getMenuByPath($path)
    {
        return $this->fetchQuery(function($query) use ($path){
            /** @var Builder $query */
            return $query
                ->where('path', $path)
                ;
        })->first();

    }

    public function deleteMenusByIds($ids)
    {
        $this->model::destroy($ids);
    }


    public function getMenusByParentId($parent_id)
    {
        return $this->fetchQuery(function($query) use ($parent_id){
            /** @var Builder $query */
            return $query
                ->where('parent_id', $parent_id)
                ->orderBy('order_no', 'asc');
        })->get();

    }

    public function totalMenus()
    {
        return $this->model::query()->count();
    }

    public function countMenusByParent($parent_id = null)
    {
        return $this->fetchQuery(function($query) use ($parent_id){
            /** @var Builder $query */
            return $query
                ->where('parent_id', $parent_id)
                ->orderBy('order_no', 'asc');
        })->count();

    }
}