<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 30 Sep 2019
 * Time: 2:15 PM
 */

namespace Modules\Apps\Repositories;


use Modules\Apps\Models\SectoralWork;
use Modules\System\Repositories\BaseRepository;

class SectoralWorkRepository extends BaseRepository
{

    /**
     * SectoralWorkRepository constructor.
     * @param SectoralWork $sectoralWork
     * @throws \Exception
     */
    public function __construct(SectoralWork $sectoralWork)
    {
        $this->setModel($sectoralWork);
    }
}