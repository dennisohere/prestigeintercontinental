<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Models\People;
use Modules\Apps\Repositories\PeopleRepository;

class PeopleController extends Controller
{
    public function index()
    {
        $peoples = PeopleRepository::init()->fetch();
        $pageTitle = 'Manage People / Team';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = PeopleRepository::init()->getModelById($edit_id);
        }

        return view('apps::backend.peoples.index', compact('peoples', 'pageTitle', 'edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes',
            'names' => 'bail|required|string',
            'position' => 'bail|nullable|string',
            'bio' => 'bail|required|string',
            'image' => 'bail|nullable|image',
        ]);

        PeopleRepository::init()
            ->initSave($payload)
            ->fillModel(function(People $model, array $data){
                return  $model->fill([
                    'names' => $data['names'],
                    'position' => $data['position'],
                    'bio' => $data['bio'],
                ]);
            })
            ->doUpload([
                'image' => 'image'
            ])
            ->persist()
        ;

        flash()->success("Saved!'");

        return redirect()->route('backend.apps.peoples.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete()
    {
        PeopleRepository::init()->deleteById(\request()->get('id'));

        flash()->info('Deleted!');

        return redirect()->back();
    }
}
