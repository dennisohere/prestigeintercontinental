<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Models\Menu;
use Modules\Apps\Repositories\MenuRepository;

class MenuController extends Controller
{
    public function index()
    {
        $menus = MenuRepository::init()->getByMainMenus();
        $pageTitle = 'Manage Menus';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = MenuRepository::init()->getMenuById($edit_id);
        }

        $parent_id = \request()->get('view_sub');
        $sub_menus = [];

        if($parent_id){
            $sub_menus = MenuRepository::init()->getMenusByParentId($parent_id);
        }
        return view('apps::backend.menus.index', compact('menus', 'edit', 'pageTitle', 'parent_id', 'sub_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function saveMenu(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes|numeric',
            'title' => 'bail|required|string',
            'link' => 'bail|nullable|string',
            'path' => 'bail|nullable|string',
            'page' => 'bail|nullable|string',
            'order_number' => 'bail|nullable|numeric',
            'parent' => 'bail|nullable|numeric',
        ]);

        MenuRepository::init()->saveMenu($payload);

        flash()->success('Saved successfully!');

        $request_args = [];

        if($request->has('view_sub')) $request_args['view_sub'] = $request->get('view_sub');

        return redirect()->route('backend.apps.menu.index')->with($request_args);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteMenu(Request $request)
    {
        MenuRepository::init()->deleteById($request->get('id'));

        flash()->warning('Menu has been deleted');

        return redirect()->back();
    }
}
