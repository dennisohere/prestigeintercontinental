<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Repositories\PartnerRepository;

class PartnerController extends Controller
{
    public function index()
    {
        $partners = PartnerRepository::init()->fetch();
        $pageTitle = 'Manage Partner';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = PartnerRepository::init()->getModelById($edit_id);
        }

        return view('apps::backend.partners.index', compact('partners', 'pageTitle', 'edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes',
            'image' => 'bail|required|image',
        ]);

        PartnerRepository::init()
            ->initSave($payload)
            ->doUpload([
                'image' => 'image'
            ])
            ->persist()
            ;

        flash()->success("Saved!'");

        return redirect()->route('backend.apps.partners.index');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete()
    {
        PartnerRepository::init()->deleteById(\request()->get('id'));

        flash()->info('Deleted!');

        return redirect()->back();
    }
}
