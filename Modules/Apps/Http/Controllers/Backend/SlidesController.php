<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Repositories\SlidesRepository;

class SlidesController extends Controller
{
    public function index()
    {
        $slides = SlidesRepository::init()->getSlides();
        $pageTitle = 'Manage Slides';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = SlidesRepository::init()->getModelById($edit_id);
        }

        return view('apps::backend.slides.index', compact('slides', 'pageTitle', 'edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function saveSlide(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes',
            'title' => 'bail|required',
            'caption' => 'bail|nullable',
            'link' => 'bail|nullable',
            'image' => 'bail|nullable|image'
        ]);

        $slide = SlidesRepository::init()->saveSlide($payload);

        flash()->success("Slider: '$slide->title' has been successfully saved!'");

        return redirect()->route('backend.apps.slides.index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteSlide(Request $request)
    {
        SlidesRepository::init()->deleteById($request->get('id'));

        flash()->info('Deleted!');

        return redirect()->back();
    }
}
