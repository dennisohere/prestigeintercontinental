<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Repositories\SectoralWorkRepository;

class SectoralWorksController extends Controller
{
    public function index()
    {
        $sectoral_works = SectoralWorkRepository::init()->fetch();
        $pageTitle = 'Manage Sectoral Works';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = SectoralWorkRepository::init()->getModelById($edit_id);
        }

        return view('apps::backend.sectoral-works.index', compact('sectoral_works', 'pageTitle', 'edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function save(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes',
            'title' => 'bail|required',
            'description' => 'bail|required',
        ]);

        SectoralWorkRepository::init()
            ->quickSave($payload)
        ;

        flash()->success("Saved!'");

        return redirect()->route('backend.apps.sectoral-works.index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function delete(Request $request)
    {
        SectoralWorkRepository::init()->deleteById($request->get('id'));

        flash()->info('Deleted!');

        return redirect()->back();
    }
}
