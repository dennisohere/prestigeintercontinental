<?php

namespace Modules\Apps\Http\Controllers\Backend;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Apps\Models\ProjectCategory;
use Modules\Apps\Repositories\ProjectCategoryRepository;
use Modules\Apps\Repositories\ProjectRepository;

class ProjectController extends Controller
{
    public function index()
    {
        $projects = ProjectRepository::init()->getProjects();
        $pageTitle = 'Manage Projects';

        $edit = null;
        if($edit_id = \request()->get('edit')){
            $edit = ProjectRepository::init()->getProjectById($edit_id);
        }

        return view('apps::backend.projects.index', compact('projects', 'pageTitle', 'edit'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Exception
     */
    public function saveProject(Request $request)
    {
        $payload = $this->validate($request, [
            'id' => 'bail|sometimes',
            'title' => 'bail|required',
            'category' => 'bail|required',
            'image' => 'bail|nullable|image',
        ]);

        $category = ProjectCategoryRepository::init()->getCategoryById($payload['category']);

        ProjectRepository::init()->saveProject($category, $payload);

        flash()->success("Saved!'");

        return redirect()->route('backend.apps.projects.index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function deleteProject(Request $request)
    {
        ProjectRepository::init()->deleteById($request->get('id'));

        flash()->info('Deleted!');

        return redirect()->back();
    }
}
