<?php

namespace Modules\Apps\Providers;

use Caffeinated\Modules\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the module services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(module_path('apps', 'Resources/Lang', 'app'), 'apps');
        $this->loadViewsFrom(module_path('apps', 'Resources/Views', 'app'), 'apps');
        $this->loadMigrationsFrom(module_path('apps', 'Database/Migrations', 'app'), 'apps');
        $this->loadConfigsFrom(module_path('apps', 'Config', 'app'));
        $this->loadFactoriesFrom(module_path('apps', 'Database/Factories', 'app'));
    }

    /**
     * Register the module services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(RouteServiceProvider::class);
    }
}
