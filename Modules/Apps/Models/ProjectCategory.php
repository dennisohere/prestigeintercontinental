<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;

class ProjectCategory extends Model
{
    protected $table = 'project_categories';

    protected $guarded = ['id'];

    public function projects()
    {
        return $this->hasMany(Project::class,'project_category_id');
    }
}
