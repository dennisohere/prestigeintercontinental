<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Modules\System\Traits\HasAvatar;
use Modules\System\Traits\HasMetadataAttribute;

class People extends Model
{
    use HasMetadataAttribute, HasAvatar;

    protected $table = 'people';

    protected $guarded = ['id'];

    public function getExcerpt()
    {
        return $this->excerpt ?? Str::limit($this->bio, '200');
    }
}
