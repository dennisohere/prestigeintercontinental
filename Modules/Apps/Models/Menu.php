<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Pages\Repositories\PagesRepository;
use Modules\System\Traits\HasMetadataAttribute;
use Modules\System\Traits\OrderNoTrait;

class Menu extends Model
{
    use HasMetadataAttribute, OrderNoTrait;

    protected $table = 'menus';

    protected $guarded = ['id'];

    public function parent_menu()
    {
        return $this->belongsTo(self::class,'parent_id');
    }

    public function child_menus()
    {
        return $this->hasMany(self::class,'parent_id');
    }

    public function has_children()
    {
        return $this->child_menus()->count() > 0;
    }

    public function getChildren()
    {
        return $this->child_menus()->orderBy('order_no')->get();
    }

    public function getAccessibleLink()
    {
        return $this->external_url ?? $this->path;
    }

    public function getPageForMenu()
    {
        if(!$page_dir = $this->page_dir) return null;

        return PagesRepository::init()->getPageFromDirectory($page_dir);
    }
}
