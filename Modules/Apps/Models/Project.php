<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\Backend\Repositories\SettingsRepository;
use Modules\System\Traits\HasAvatar;
use Modules\System\Traits\HasMetadataAttribute;

class Project extends Model
{
    use HasMetadataAttribute, HasAvatar;

    protected $table = 'projects';

    protected $guarded = ['id'];

    public function category()
    {
        return $this->belongsTo(ProjectCategory::class,'project_category_id');
    }

    public function getCoverImage()
    {
        return $this->getAvatar('cover_image');
    }
}
