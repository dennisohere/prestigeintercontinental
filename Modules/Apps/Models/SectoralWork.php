<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class SectoralWork extends Model
{
    use HasMetadataAttribute;

    protected $table = 'sectoral_works';

    protected $guarded = ['id'];


}
