<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class Slide extends Model
{
    use HasMetadataAttribute;

    protected $table = 'slides';

    protected $guarded = ['id'];

    public function getImageUploadStoragePath()
    {
        return $this->metadata['image_url']['path'] ?? null;
    }

    public function getVideoUploadStoragePath()
    {
        return $this->metadata['video']['path'] ?? null;
    }
}
