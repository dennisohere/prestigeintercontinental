<?php

namespace Modules\Apps\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasAvatar;
use Modules\System\Traits\HasMetadataAttribute;

class Partner extends Model
{
    use HasMetadataAttribute, HasAvatar;

    protected $table = 'partners';

    protected $guarded = ['id'];

    public function getCoverImage()
    {
        return $this->getAvatar();
    }
}
