@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Sectoral Works</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Sectoral Work
                            </button>
                        </div>
                    </div>
                </div>

                @if($sectoral_works->count() > 0)

                    <div class="card-body row">
                        @foreach($sectoral_works as $sectoral_work)
                            <div class="col-lg-6">
                                <div class="card shadow--hover">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            {{$sectoral_work->title}}
                                        </h5>
                                        <p>
                                            {{$sectoral_work->description}}
                                        </p>
                                    </div>
                                    <div class="card-footer d-flex justify-content-between">
                                        <div>
                                            <form action="{{route('backend.apps.sectoral-works.delete')}}"
                                                  class="d-inline"
                                                  method="post" id="deleteForm_{{$sectoral_work->id}}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$sectoral_work->id}}">
                                                <button class="btn btn-sm btn-danger" type="submit"
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="ni ni-basket"></i>
                                                    Delete
                                                </button>
                                            </form>
                                            <a href="?edit={{$sectoral_work->id}}" class="btn btn-outline-primary btn-sm">
                                                <i class="fa fa-pencil"></i>
                                                Edit
                                            </a>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>
    </div>

    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Sectoral Work...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form action="{{route('backend.apps.sectoral-works.save')}}" method="post" class="m-t-20"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="form-control-label">
                                Title
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                   placeholder="" name="title" >
                        </div>


                        <div class="form-group">
                            <label for="description" class="form-control-label">
                                Description
                            </label>
                            <textarea name="description" id="description" cols="" rows="10" class="form-control form-control-sm"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>

        </div>

    </div>


    @if($edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Sectoral Work...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <form action="{{route('backend.apps.sectoral-works.save')}}" method="post" class="m-t-20"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="names" class="form-control-label">
                                    Title
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="names"
                                       placeholder="" name="title" value="{{$edit->title}}">
                            </div>

                            <div class="form-group">
                                <label for="names" class="form-control-label">
                                    Description
                                </label>
                                <textarea name="description" id="description" cols="" rows="10"
                                          class="form-control form-control-sm">{{$edit->description}}</textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-primary">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>

        </div>

    @endif
@endsection


@push('scripts')



@endpush