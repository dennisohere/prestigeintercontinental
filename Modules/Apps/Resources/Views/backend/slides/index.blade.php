@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Slides</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Slide
                            </button>
                        </div>
                    </div>
                </div>

                @if($slides->count() > 0)

                    <div class="card-body row">
                        @foreach($slides as $slide)
                            <div class="col-lg-4">
                                <div class="card shadow--hover">
                                    <img src="{{$slide->image_url}}" alt="" class="card-img-top">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            {{$slide->title}}
                                        </h5>
                                        <p>
                                            {{$slide->caption}}
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <form action="{{route('backend.apps.slides.delete')}}"
                                              class="d-inline"
                                              method="post" id="deleteForm_{{$slide->id}}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{$slide->id}}">
                                            <button class="btn btn-sm btn-danger" type="submit"
                                                    onclick="return confirm('Are you sure?')">
                                                <i class="ni ni-basket"></i>
                                                Delete
                                            </button>
                                        </form>
                                        <a href="?edit={{$slide->id}}" class="btn btn-outline-primary btn-sm">
                                            <i class="fa fa-pencil"></i>
                                            Edit
                                        </a>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>
    </div>

    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Slide...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form action="{{route('backend.apps.slides.save')}}" method="post" class="m-t-20"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="form-control-label">
                                Title
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                   placeholder="" name="title" >
                        </div>

                        <div class="form-group">
                            <label for="caption" class="form-control-label">
                                Caption
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="caption"
                                   placeholder="" name="caption" >
                        </div>

                        <div class="form-group">
                            <label for="link" class="form-control-label">
                                External Link
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="link"
                                   placeholder="" name="link" >
                        </div>

                        <div class="form-group">
                            <label for="image" class="form-control-label">
                                Upload Image
                            </label>
                            <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                   accept="image/jpeg, image/png, image/jpg"
                                   placeholder="" name="image" >
                        </div>

                        {{--<div class="form-group row">
                            <label for="video" class="col-3 control-label">
                                Upload Video (<small>if any</small>)
                            </label>
                            <div class="col-9">
                                <input type="file" class="form-control form-control-sm form-control-alternative" id="video"
                                       accept="video/*"
                                       placeholder="" name="video" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 control-label">Publish</label>
                            <div class="col-9">
                                <div class="radio radio-success">
                                    <input type="radio" value="1" name="publish" id="yes">
                                    <label for="yes">Yes</label>
                                    <input type="radio" checked="checked" value="0" name="publish" id="no">
                                    <label for="no">No</label>
                                </div>
                            </div>
                        </div>
                        --}}
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>

        </div>

    </div>


    @if($edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">New Slide...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <form action="{{route('backend.apps.slides.save')}}" method="post" class="m-t-20"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="title" class="form-control-label">
                                    Title
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                       placeholder="" name="title" value="{{$edit->title}}">
                            </div>

                            <div class="form-group">
                                <label for="caption" class="form-control-label">
                                    Caption
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="caption"
                                       placeholder="" name="caption" value="{{$edit->caption}}">
                            </div>

                            <div class="form-group">
                                <label for="link" class="form-control-label">
                                    External Link
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="link"
                                       placeholder="" name="link" value="{{$edit->link}}">
                            </div>

                            <div class="form-group">
                                <label for="image" class="form-control-label">
                                    Upload Image
                                </label>
                                <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                       accept="image/jpeg, image/png, image/jpg"
                                       placeholder="" name="image" >
                            </div>

                            {{--<div class="form-group row">
                                <label for="video" class="col-3 control-label">
                                    Upload Video (<small>if any</small>)
                                </label>
                                <div class="col-9">
                                    <input type="file" class="form-control form-control-sm form-control-alternative" id="video"
                                           accept="video/*"
                                           placeholder="" name="video" >
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3 control-label">Publish</label>
                                <div class="col-9">
                                    <div class="radio radio-success">
                                        <input type="radio" value="1" name="publish" id="yes">
                                        <label for="yes">Yes</label>
                                        <input type="radio" checked="checked" value="0" name="publish" id="no">
                                        <label for="no">No</label>
                                    </div>
                                </div>
                            </div>
                            --}}
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-primary">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>

        </div>

    @endif
@endsection


@push('scripts')



@endpush