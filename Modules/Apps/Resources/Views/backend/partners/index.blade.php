@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Partners</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Partner
                            </button>
                        </div>
                    </div>
                </div>

                @if($partners->count() > 0)

                    <div class="card-body row">
                        @foreach($partners as $partner)
                            <div class="col-lg-4">
                                <div class="card shadow--hover">
                                    <img src="{{$partner->image}}" alt="" class="card-img-top">
                                    <div class="card-footer d-flex justify-content-between">
                                        <div>
                                            <form action="{{route('backend.apps.partners.delete')}}"
                                                  class="d-inline"
                                                  method="post" id="deleteForm_{{$partner->id}}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$partner->id}}">
                                                <button class="btn btn-sm btn-danger" type="submit"
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="ni ni-basket"></i>
                                                    Delete
                                                </button>
                                            </form>
                                            <a href="?edit={{$partner->id}}" class="btn btn-outline-primary btn-sm">
                                                <i class="fa fa-pencil"></i>
                                                Edit
                                            </a>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>
    </div>

    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Partner...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form action="{{route('backend.apps.partners.save')}}" method="post" class="m-t-20"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="image" class="form-control-label">
                                Upload Image
                            </label>
                            <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                   accept="image/jpeg, image/png, image/jpg"
                                   placeholder="" name="image" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>

        </div>

    </div>


    @if($edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Partner...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <form action="{{route('backend.apps.partners.save')}}" method="post" class="m-t-20"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="image" class="form-control-label">
                                    Upload Image
                                </label>
                                <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                       accept="image/jpeg, image/png, image/jpg"
                                       placeholder="" name="image" >
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-primary">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>

        </div>

    @endif
@endsection


@push('scripts')



@endpush