@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Projects</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Project
                            </button>
                        </div>
                    </div>
                </div>

                @if($projects->count() > 0)

                    <div class="card-body row">
                        @foreach($projects as $project)
                            <div class="col-lg-4">
                                <div class="card shadow--hover">
                                    <img src="{{$project->cover_image}}" alt="" class="card-img-top">
                                    <div class="card-body">
                                        <h5 class="card-title">
                                            {{$project->title}}
                                        </h5>
                                        <p>
                                            {{$project->caption}}
                                        </p>
                                    </div>
                                    <div class="card-footer d-flex justify-content-between">
                                        <div>
                                            <form action="{{route('backend.apps.projects.delete')}}"
                                                  class="d-inline"
                                                  method="post" id="deleteForm_{{$project->id}}">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$project->id}}">
                                                <button class="btn btn-sm btn-danger" type="submit"
                                                        onclick="return confirm('Are you sure?')">
                                                    <i class="ni ni-basket"></i>
                                                    Delete
                                                </button>
                                            </form>
                                            <a href="?edit={{$project->id}}" class="btn btn-outline-primary btn-sm">
                                                <i class="fa fa-pencil"></i>
                                                Edit
                                            </a>
                                        </div>
                                        <span class="badge badge-info small">
                                            {{$project->category->title}}
                                        </span>
                                    </div>
                                </div>

                            </div>
                        @endforeach
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>
    </div>

    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Project...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="pg-close fs-14"></i>
                    </button>
                </div>
                <form action="{{route('backend.apps.projects.save')}}" method="post" class="m-t-20"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="form-control-label">
                                Title
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                   placeholder="" name="title" >
                        </div>

                        <div class="form-group">
                            <label for="category" class="form-control-label">
                                Category
                            </label>
                            <select name="category" id="category" class="form-control-sm form-control">
                                <option value="">Pick a category</option>
                                @foreach(\Modules\Apps\Repositories\ProjectCategoryRepository::init()->getAllCategories() as $category)
                                    <option value="{{$category->id}}">{{$category->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="image" class="form-control-label">
                                Upload Image
                            </label>
                            <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                   accept="image/jpeg, image/png, image/jpg"
                                   placeholder="" name="image" >
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-outline-primary">
                            Submit
                        </button>
                    </div>
                </form>
            </div>

        </div>

    </div>


    @if($edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Edit Project...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            <i class="pg-close fs-14"></i>
                        </button>
                    </div>
                    <form action="{{route('backend.apps.projects.save')}}" method="post" class="m-t-20"
                          enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="title" class="form-control-label">
                                    Title
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                       placeholder="" name="title" value="{{$edit->title}}">
                            </div>

                            <div class="form-group">
                                <label for="category" class="form-control-label">
                                    Category
                                </label>
                                <select name="category" id="category" class="form-control-sm form-control">
                                    <option value="">Pick a category</option>
                                    @foreach(\Modules\Apps\Repositories\ProjectCategoryRepository::init()->getAllCategories() as $category)
                                        <option value="{{$category->id}}" {{$category->id == $edit->project_category_id ? 'selected' : ''}}>{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="image" class="form-control-label">
                                    Upload Image
                                </label>
                                <input type="file" class="form-control form-control-sm form-control-file" id="image"
                                       accept="image/jpeg, image/png, image/jpg"
                                       placeholder="" name="image" >
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-outline-primary">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>

            </div>

        </div>

    @endif
@endsection


@push('scripts')



@endpush