@extends('backend::layout.app')

@section('app')

    <div class="row">

        <div class="col-sm-12">
            <div class="card shadow">
                <div class="card-header border-0">
                    <div class="row align-items-center">
                        <div class="col">
                            <h3 class="mb-0">Menus</h3>
                        </div>
                        <div class="col text-right">
                            <button class="btn btn-sm btn-primary"
                                    data-target="#modalNew" data-toggle="modal">
                                Add Menu
                            </button>
                        </div>
                    </div>
                </div>

                @if($menus->count() > 0)

                    <div class="table-responsive">
                        <!-- Projects table -->
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                            <tr>
                                <th scope="col">Title</th>
                                <th scope="col">Route</th>
                                <th scope="col">Page</th>
                                <th scope="col"></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($menus as $menu)
                                <tr>
                                    <td>
                                        {{$menu->title}}
                                    </td>
                                    <th scope="row">
                                        {{$menu->getAccessibleLink()}}
                                    </th>
                                    <td>
                                        @if($page = $menu->getPageForMenu())
                                            {{$page->getPageTitle()}}
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                                id="dropdownMenuButton-{{$menu->id}}" data-toggle="dropdown"
                                                aria-haspopup="true" aria-expanded="false">
                                            Action
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-{{$menu->id}}">
                                            <a class="dropdown-item"
                                               href="{{route('backend.apps.menu.index', ['edit' => $menu->id])}}">
                                                Edit Menu
                                            </a>

                                            @if($menu->has_children())
                                                <a class="dropdown-item"
                                                   href="{{route('backend.apps.menu.index', ['view_sub' => $menu->id])}}">
                                                    View Sub Menus
                                                </a>
                                            @endif

                                            <form action="{{route('backend.apps.menu.delete')}}"
                                                  id="deleteForm_{{$menu->id}}" class="form-inline mt-3" method="post">
                                                @csrf
                                                <input type="hidden" name="ids" value="{{$menu->id}}">
                                                <button class="btn btn-sm btn-danger btn-block" type="submit" onclick="return confirm('Are you sure?')">
                                                    Delete
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                                @if($parent_id == $menu->id)
                                @foreach($sub_menus as $sub_menu)
                                    <tr class="bg-lighter">
                                        <td>
                                            &mdash;&mdash;{{$sub_menu->title}}
                                        </td>
                                        <th scope="row">
                                            &mdash;&mdash;
                                            {{$sub_menu->getAccessibleLink()}}
                                        </th>
                                        <td>
                                            @if($page = $sub_menu->getPageForMenu())
                                                {{$page->getPageTitle()}}
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-secondary btn-sm dropdown-toggle" type="button"
                                                    id="dropdownMenuButton-sub-{{$sub_menu->id}}" data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">
                                                Action
                                            </button>
                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton-sub-{{$sub_menu->id}}">
                                                <a class="dropdown-item"
                                                   href="{{route('backend.apps.menu.index', ['edit' => $sub_menu->id])}}">
                                                    Edit Menu
                                                </a>

                                                <form action="{{route('backend.apps.menu.delete')}}"
                                                      id="deleteForm_sub_{{$sub_menu->id}}" class="form-inline mt-3" method="post">
                                                    @csrf
                                                    <input type="hidden" name="ids" value="{{$sub_menu->id}}">
                                                    <button class="btn btn-sm btn-danger btn-block" type="submit"
                                                            onclick="return confirm('Are you sure?')">
                                                        Delete
                                                    </button>
                                                </form>
                                            </div>

                                        </td>
                                    </tr>
                                @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                @else

                    <div class="card-body">
                        <p class="card-title text-center text-warning">No Data Available.</p>
                    </div>

                @endif

            </div>
        </div>

    </div>


    <div class="modal fade stick-up" id="modalNew" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add New Menu...</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{route('backend.apps.menu.save')}}" method="post" class="m-t-20"
                      enctype="multipart/form-data">
                    @if(request()->has('view_sub'))
                        <input type="hidden" name="view_sub" value="{{request()->get('view_sub')}}">
                    @endif
                    @csrf
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="title" class="form-control-label">
                                Title
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                   placeholder="" name="title" >
                        </div>

                        <div class="form-group">
                            <label for="parent" class="form-control-label">
                                Select Page
                            </label>
                            <select id="parent" class="form-control form-control-sm form-control-alternative" name="page">
                                <option value="">Choose...</option>
                                @foreach(\Modules\Pages\Repositories\PagesRepository::init()->getPages() as $page)
                                    <option value="{{$page->getKey()}}">{{$page->getPageTitle()}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="path" class="form-control-label">
                                Path
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="path"
                                   placeholder="URL Path for Menu" name="path" >
                        </div>

                        <div class="form-group">
                            <label for="link" class="form-control-label">
                                Link
                            </label>
                            <input type="text" class="form-control form-control-sm form-control-alternative" id="link"
                                   placeholder="" name="link" >
                        </div>

                        <div class="form-group">
                            <label for="parent" class="form-control-label">
                                Select Parent menu
                            </label>
                            <select id="parent" class="form-control form-control-sm form-control-alternative" name="parent">
                                <option value="">Choose...</option>
                                @foreach(\Modules\Apps\Repositories\MenuRepository::init()->getOnlyParentMenus() as $menu)
                                    <option value="{{$menu->id}}">{{$menu->title}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="order_number" class="form-control-label">
                                Set order:
                            </label>
                            <select id="order_number" class="form-control form-control-sm form-control-alternative" name="order_number">
                                @for($i=1; $i<= \Modules\Apps\Repositories\MenuRepository::init()->totalMenus() + 1; $i++)
                                    <option value="{{$i}}">{{$i}}</option>
                                @endfor
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-outline-primary btn-sm">
                                Submit
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>

    @if(isset($edit) && $edit)

        <div class="modal fade stick-up" id="modalEdit" tabindex="-1" role="dialog" aria-hidden="true">

            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Add New Menu...</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <form action="{{route('backend.apps.menu.save')}}" method="post" class="m-t-20"
                          enctype="multipart/form-data">
                        <input type="hidden" name="id" value="{{$edit->id}}">
                        @if(request()->has('view_sub'))
                            <input type="hidden" name="view_sub" value="{{request()->get('view_sub')}}">
                        @endif
                        @csrf
                        <div class="modal-body">

                            <div class="form-group">
                                <label for="title" class="form-control-label">
                                    Title
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="title"
                                       placeholder="" name="title" value="{{$edit->title}}">
                            </div>

                            <div class="form-group">
                                <label for="parent" class="form-control-label">
                                    Select Page
                                </label>
                                <select id="parent" class="form-control form-control-sm form-control-alternative" name="page">
                                    <option value="">Choose...</option>
                                    @foreach(\Modules\Pages\Repositories\PagesRepository::init()->getPages() as $page)
                                        <option value="{{$page->getKey()}}" {{$edit->page_dir == $page->getKey() ? 'selected' : ''}}>{{$page->getPageTitle()}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="path" class="form-control-label">
                                    Path
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="path"
                                       placeholder="URL Path for Menu" name="path" value="{{$edit->path}}">
                            </div>

                            <div class="form-group">
                                <label for="link" class="form-control-label">
                                    Link
                                </label>
                                <input type="text" class="form-control form-control-sm form-control-alternative" id="link"
                                       placeholder="" name="link" value="{{$edit->external_url}}">
                            </div>

                            <div class="form-group">
                                <label for="parent" class="form-control-label">
                                    Select Parent menu
                                </label>
                                <select id="parent" class="form-control form-control-sm form-control-alternative" name="parent">
                                    <option value="">Choose...</option>
                                    @foreach(\Modules\Apps\Repositories\MenuRepository::init()->getOnlyParentMenus() as $parentMenu)
                                        <option value="{{$parentMenu->id}}" {{$edit->parent_id == $parentMenu->id ? 'selected' : ''}}>{{$parentMenu->title}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="order_number" class="form-control-label">
                                    Set order:
                                </label>
                                <select id="order_number" class="form-control form-control-sm form-control-alternative" name="order_number">
                                    @for($i=1; $i<= \Modules\Apps\Repositories\MenuRepository::init()->countMenusByParent(); $i++)
                                        <option value="{{$i}}" {{$edit->order_no == $i ? 'selected' : ''}}>{{$i}}</option>
                                    @endfor
                                </select>
                            </div>

                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btn-outline-primary btn-sm">
                                    Submit
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>


        </div>

    @endif

@endsection


@push('scripts')



@endpush