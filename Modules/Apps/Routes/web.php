<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('backend/apps')->name('backend.apps.')->middleware('auth')->namespace('Backend')->group(function () {

    /*
     * slides routes
     */
    Route::prefix('slides')->name('slides.')->group(function (){
        Route::get('', 'SlidesController@index')->name('index');
        Route::post('save', 'SlidesController@saveSlide')->name('save');
        Route::post('delete', 'SlidesController@deleteSlide')->name('delete');
    });

    /*
     * menu routes
     */
    Route::prefix('menu')->name('menu.')->group(function () {
        Route::get('/', 'MenuController@index')->name('index');
        Route::post('save', 'MenuController@saveMenu')->name('save');
        Route::post('delete', 'MenuController@deleteMenu')->name('delete');
    });

    /*
     * projects routes
     */
    Route::prefix('projects')->name('projects.')->group(function (){
        Route::get('', 'ProjectController@index')->name('index');
        Route::post('save', 'ProjectController@saveProject')->name('save');
        Route::post('delete', 'ProjectController@deleteProject')->name('delete');
    });

    /*
     * partners routes
     */
    Route::prefix('partners')->name('partners.')->group(function (){
        Route::get('', 'PartnerController@index')->name('index');
        Route::post('save', 'PartnerController@save')->name('save');
        Route::post('delete', 'PartnerController@delete')->name('delete');
    });

    /*
     * peoples routes
     */
    Route::prefix('peoples')->name('peoples.')->group(function (){
        Route::get('', 'PeopleController@index')->name('index');
        Route::post('save', 'PeopleController@save')->name('save');
        Route::post('delete', 'PeopleController@delete')->name('delete');
    });

    /*
     * peoples routes
     */
    Route::prefix('sectoral-works')->name('sectoral-works.')->group(function (){
        Route::get('', 'SectoralWorksController@index')->name('index');
        Route::post('save', 'SectoralWorksController@save')->name('save');
        Route::post('delete', 'SectoralWorksController@delete')->name('delete');
    });
});
