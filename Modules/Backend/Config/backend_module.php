<?php
/**
 * Created by PhpStorm.
 * User: Dennis
 * Date: 8/14/2019
 * Time: 10:56 PM
 */

return [
    'roles' => [
        'dev' => 'Developer',
        'content_officer' => 'Content Officer',
        'super_admin' => 'Super Admin',
    ],
    'default_password' => '1234567890'
];