<?php

namespace Modules\Backend\Models;

use Illuminate\Database\Eloquent\Model;
use Modules\System\Traits\HasMetadataAttribute;

class Settings extends Model
{
    use HasMetadataAttribute;

    protected $table = 'settings';

    protected $guarded = ['id'];


    public function getLogoImageUploadStoragePath()
    {
        return $this->metadata['path'];
    }
}
