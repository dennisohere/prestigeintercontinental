<?php

namespace Modules\Backend\Repositories;


use Modules\Backend\Models\Settings;
use Modules\System\Traits\SystemRepositoryTrait;

class SettingsRepository
{
    use SystemRepositoryTrait;

    /**
     * @var Settings
     */
    private $settings;


    /**
     * SettingsRepository constructor.
     * @param Settings $settings
     */
    public function __construct(Settings $settings)
    {
        $this->settings = $settings;
    }

    public function setLogo()
    {
        if ($uploaded = $this->autoSaveFileFromRequest('image')){

            $this->saveSettings('SITE_LOGO', $uploaded['url'], $uploaded);
        }
    }

    public function getLogo()
    {
        return $this->getSettingsValue('SITE_LOGO') ?? asset('assets/images/image_placeholder.png');
    }

    /**
     * @param $key
     * @param $value
     * @param array $metadata
     * @return Settings
     */
    public function saveSettings($key, $value, array $metadata = [])
    {
        $settings = $this->getSettingsByKey($key);

        if(!$settings) $settings = $this->settings->newInstance([
            'key' => $key
        ]);

        $settings->fill([
            'value' => $value
        ]);

        if(!empty($metadata)){
            $settings->fill([
                'metadata' => json_encode($metadata)
            ]);
        }

        $settings->save();

        return $settings;
    }

    /**
     * @param $key
     * @return Settings
     */
    public function getSettingsByKey($key)
    {
        return $this->settings->newModelQuery()->where('key', $key)->first();
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function getSettingsValue($key)
    {
        $settings = $this->getSettingsByKey($key);

        return $settings ? $settings->value : null;
    }

}