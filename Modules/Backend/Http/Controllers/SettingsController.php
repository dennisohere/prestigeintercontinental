<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Modules\Backend\Models\User;
use Modules\Backend\Repositories\SettingsRepository;

class SettingsController extends Controller
{
    public function index()
    {
        $pageTitle = 'Settings';

        return view('backend::settings.index', compact('pageTitle'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function changePassword(Request $request)
    {
        $payload = $this->validate($request, [
            'current_password' => 'bail|required',
            'new_password' => 'bail|required|same:retype_password',
        ]);

        /** @var User $user */
        $user = auth()->user();

        $result = $user->changeUserPassword($payload['current_password'], $payload['new_password']);

        if($result){
            flash()->success('Password was successfully updated');
        } else {
            flash()->error('Error! Password was not updated!');
        }

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function saveSiteLogo(Request $request)
    {
        $this->validate($request, [
           'image' => 'bail|required|image'
        ]);

        SettingsRepository::init()->setLogo();

        flash()->message('Saved');

        return redirect()->back();
    }
}
