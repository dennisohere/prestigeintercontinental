<?php

namespace Modules\Backend\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/backend/dashboard';

    public function showLoginForm()
    {
        return view('backend::auth.login');
    }

    public function loggedOut(Request $request)
    {
        session()->forget('auth-token');

        return redirect()->route('backend.auth.login');
    }
}
