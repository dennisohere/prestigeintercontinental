<?php

namespace Modules\Backend\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Modules\Backend\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::truncate();

        /** @var User $dev */
        $dev = User::create([
            'first_name' => 'Dennis',
            'last_name' => 'O',
           'email' => 'dennisohere@live.com',
           'password' => bcrypt('secret'),
//            'api_token' => Str::random(60),
        ]);
        $dev->assignRole(config('backend_module.roles.dev'));

        /** @var User $super_admin */
        $super_admin = User::create([
            'first_name' => 'Super',
            'last_name' => 'Admin',
            'email' => 'superadmin@site.com',
            'password' => bcrypt('1234567890'),
//            'api_token' => Str::random(60),
        ]);
        $super_admin->assignRole(config('backend_module.roles.super_admin'));

        /** @var User $content_officer */
        $content_officer = User::create([
            'first_name' => 'Content',
            'last_name' => 'Admin',
            'email' => 'content.admin@site.com',
            'password' => bcrypt('1234567890'),
//            'api_token' => Str::random(60),
        ]);
        $content_officer->assignRole(config('backend_module.roles.content_officer'));
    }
}
