<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your module. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('backend')->middleware('auth')->name('backend.')->group(function () {
    Route::get('/', 'DashboardController@dashboard')->name('dashboard');
    Route::get('dashboard', 'DashboardController@dashboard')->name('dashboard');


    Route::post('logout', 'Auth\LoginController@logout')->middleware('auth')->name('logout');

    Route::prefix('settings')->name('settings.')->group(function () {
        Route::get('/', 'SettingsController@index')->name('index');
        Route::post('save/logo', 'SettingsController@saveSiteLogo')->name('save.logo');
        Route::post('save/password', 'SettingsController@changePassword')->name('save.password');

    });
});

Route::prefix('backend')->name('backend.auth.')->namespace('Auth')->group(function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login')->name('login');

});
