<!-- Navigation -->
<ul class="navbar-nav">
    <li class="nav-item active" >
        <a class=" nav-link active " href="/backend/dashboard"> <i class="ni ni-tv-2 text-primary"></i> Dashboard
        </a>
    </li>
    <li class="nav-item" >
        <a class=" nav-link " href="{{route('backend.pages.index')}}"> <i class="ni ni-books text-primary"></i> Web Pages</a>
    </li>
    <li class="nav-item" >
        <a class=" nav-link " href="{{route('backend.settings.index')}}"> <i class="ni ni-settings-gear-65 text-primary"></i> Settings</a>
    </li>
</ul>
<!-- Divider -->
<hr class="my-3">
<!-- Heading -->
<h6 class="navbar-heading text-muted">Manage Data</h6>
<!-- Navigation -->
<ul class="navbar-nav mb-md-3">
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.menu.index')}}">
            <i class="ni ni-bullet-list-67"></i> Menus
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.slides.index')}}">
            <i class="ni ni-image"></i> Home Slides
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.sectoral-works.index')}}">
            <i class="ni ni-world"></i> Sectoral Works
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.projects.index')}}">
            <i class="ni ni-briefcase-24"></i> Projects
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.partners.index')}}">
            <i class="ni ni-world"></i> Partners
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{route('backend.apps.peoples.index')}}">
            <i class="ni ni-circle-08"></i> People / Team
        </a>
    </li>
</ul>