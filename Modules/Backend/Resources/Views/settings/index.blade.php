@extends('backend::layout.app')

@section('app')

    <div class="row">
        <div class="col-xl-4">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-muted ls-1 mb-1">Site Settings</h6>
                            <h2 class="mb-0">Website Logo</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-12">
                            <div class="text-center">
                                <img src="{{\Modules\Backend\Repositories\SettingsRepository::init()->getLogo()}}" class="rounded img-thumbnail" alt="...">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('backend.settings.save.logo')}}" class="" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group row">
                                    <div class="col-12 mb-2">
                                        <input type="file" class="form-control" name="image">
                                    </div>
                                    <div class="col-12">
                                        <button class="btn btn-block btn-primary btn-sm">Upload</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-6 offset-xl-2 mb-5 mb-xl-0">
            <div class="card shadow">
                <div class="card-header bg-transparent">
                    <div class="row align-items-center">
                        <div class="col">
                            <h6 class="text-uppercase text-muted ls-1 mb-1">Account Settings</h6>
                            <h2 class="mb-0">Change Password</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form action="{{route('backend.settings.save.password')}}" method="post">
                        @csrf

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label text-right">Current Password:</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control form-control-round"
                                       name="current_password"
                                       placeholder="Your current password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label text-right">New Password:</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control form-control-round"
                                       name="new_password"
                                       placeholder="Enter new password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label text-right">Retype Password:</label>
                            <div class="col-sm-7">
                                <input type="password" class="form-control form-control-round"
                                       name="retype_password"
                                       placeholder="Retype new password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-7 offset-5">
                                <button type="submit" class="btn btn-outline-success btn-sm btn-block btn-round">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('app-header')


@endsection