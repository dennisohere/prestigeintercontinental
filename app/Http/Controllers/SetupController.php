<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class SetupController extends Controller
{
    public function storage()
    {
        Artisan::call('storage:link');
    }

    public function install()
    {
        Artisan::call('system:install');
    }

    public function live()
    {
        Artisan::call('optimize');
    }

    public function clearOptimization()
    {
        Artisan::call('optimize:clear');
    }
}
